#ifndef CB_H
  #define CB_H
  #define IN_COMMAND_BUF_LEN 255                  //����� ���������� ������ �������� ������
  #define BUFFER_OVERROUNDED 0x01                 //1 ���: 0 - �� ��� �� ������� ����; 1 - �� ������� ����
  #define PUT_OVERROUNDED 0x02                    //2 ���: 0 - Put ������� ���� (������ ��� Get); 1 - Put � Get � ����� ����� (������ Put > ������ Get)

  int cb_getInd_inc( int inc);                    //implemented in main.c
  int putchar_nocheck( int ch);                   //implemented in main.c
#endif
