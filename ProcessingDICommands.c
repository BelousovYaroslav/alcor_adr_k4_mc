#include <ADuC7026.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "logger.h"
#include "cb.h"
#include "checksumm.h"
#include "leds.h"
#include "settings.h"

extern unsigned char gl_cAddrDigitalInput;     //адрес блока цифровых выходов    (реле выход) TU  DO
extern unsigned char gl_cCheckSummUsageDI;      //использование CheckSumm

extern char *gl_cpCircleBuffer;                 //кольцевой буфер байт входящих команд
extern int  gl_nCircleBufferPut;                //индекс где мы можем положить следующий входящий байт
extern int  gl_nCircleBufferGet;                //индекс указывающий последний взятый символ
extern char gl_bCircleBufferSettings;           //флаги кольцевого буфера
extern char gl_bIncomingCommand;                //Флаг принятой входящей команды

//обработка команд цифровых входов
void ProcessDICommand( int nFirstCommandLen) {
  char strResponse[256];
  unsigned short ushResponse;
  //int i;
  //unsigned char cCS = 0;
  
  memset( strResponse, 0, 256);
  
  LogDebug( "DI: in with %d", nFirstCommandLen);

  //INCOMING CHECKSUM VERIFICATION
  if( gl_cCheckSummUsageDI && !ValidateCheckSumm( nFirstCommandLen)) {
    LedOn( LED_ERR);
    LogWarn( "DI: Incoming CheckSumm verification fail!");
    return;
  }

  if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '$' ) {

    // **************************************************************************************
    //  ${AA}6
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '6' ) {

      if( ( gl_cCheckSummUsageDI  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageDI && nFirstCommandLen == 5)) {

        LogDebug( "DI: Recognized ${AA}6: Digital Data In request command ");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        ushResponse = 0;

        if( GP3DAT & 0x80) ushResponse += 0x0001;     //TS01 = P3.7
        if( GP2DAT & 0x80) ushResponse += 0x0002;     //TS02 = P2.7
        if( GP2DAT & 0x02) ushResponse += 0x0004;     //TS03 = P2.1
        if( GP2DAT & 0x04) ushResponse += 0x0008;     //TS04 = P2.2

        if( GP1DAT & 0x80) ushResponse += 0x0010;     //TS05 = P1.7
        if( GP1DAT & 0x40) ushResponse += 0x0020;     //TS06 = P1.6
        if( GP4DAT & 0x01) ushResponse += 0x0040;     //TS07 = P4.0
        if( GP4DAT & 0x02) ushResponse += 0x0080;     //TS08 = P4.1

        if( GP1DAT & 0x20) ushResponse += 0x0100;     //TS09 = P1.5
        if( GP1DAT & 0x10) ushResponse += 0x0200;     //TS10 = P1.4
        if( GP1DAT & 0x08) ushResponse += 0x0400;     //TS11 = P1.3
        if( GP1DAT & 0x04) ushResponse += 0x0800;     //TS12 = P1.2

        if( GP4DAT & 0x04) ushResponse += 0x1000;     //TS13 = P4.2
        if( GP4DAT & 0x08) ushResponse += 0x2000;     //TS14 = P4.3
        if( GP4DAT & 0x10) ushResponse += 0x4000;     //TS15 = P4.4
        if( GP4DAT & 0x20) ushResponse += 0x8000;     //TS16 = P4.5

        strResponse[ 0] = '!';
        sprintf( &strResponse[1], "%04X", ushResponse);

        if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "DI: Wrong formatted ${AA}6 command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);
        
        //RESPONSE.PREPARATION
        strResponse[ 0] = '?';
        sprintf( &strResponse[1], "%02d", gl_cAddrDigitalInput);
        if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }

    else
    // **************************************************************************************
    //  ${AA}M
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'M' ) {

      if( ( gl_cCheckSummUsageDI  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageDI && nFirstCommandLen == 5)) {

        LogDebug( "DI: Get module name command ${AA}M");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "!%c%cGPI",
                      gl_cpCircleBuffer[ cb_getInd_inc( 2)],
                      gl_cpCircleBuffer[ cb_getInd_inc( 3)]);

        if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "DI: Wrong formatted ${AA}M command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrDigitalInput);
        if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }

    }
    
    else
    // **************************************************************************************
    //  ${AA}2
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '2' ) {
      
      if( ( gl_cCheckSummUsageDI  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageDI && nFirstCommandLen == 5)) {
            
        LogDebug( "AI: Recognized ${AA}2: Config request command ");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "!%c%c0000%c0", 
                      gl_cpCircleBuffer[ cb_getInd_inc( 2)],    //%c AA
                      gl_cpCircleBuffer[ cb_getInd_inc( 3)],    //%c AA
                                                                //0  TT
                                                                //0  TT
                                                                //0  CC
                                                                //0  CC
                      gl_cCheckSummUsageDI ? '4':'0');          //%c FF
                                                                //0  FF

        if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();
        
        return;
      }
      else {
        LogInfo( "AO: Wrong formatted ${AA}2 command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrDigitalInput);
        if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }

    }

    LogDebug( "DI: Unknown $command.");
    //COMMAND.NOT.ACCEPTED
    LedOn( LED_ERR);

    //RESPONSE.PREPARATION
    sprintf( strResponse, "?%02X", gl_cAddrDigitalInput);
    if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

    //RESPONSE.OUT
    LedOn( LED_TX);
    RS485_SetToTransmit();
    printf( "%s\r", strResponse);
    RS485_SetToReceive();
    
    return;
  }

  LogDebug( "DI: Unknown command. Does not starts with $");
  //COMMAND.NOT.ACCEPTED
  LedOn( LED_ERR);

  //RESPONSE.PREPARATION
  sprintf( strResponse, "?%02X", gl_cAddrDigitalInput);
  if( gl_cCheckSummUsageDI) AddCheckSumm( strResponse);

  //RESPONSE.OUT
  LedOn( LED_TX);
  RS485_SetToTransmit();
  printf( "%s\r", strResponse);
  RS485_SetToReceive();
}
