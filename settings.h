#ifndef SETTINGS_H
#define SETTINGS_H

#define LED_ERR 0
#define LED_RX  1
#define LED_TX  2

void load_params( void);                        //загрузить параметры из флэш-памяти в переменные
void save_params( int nMask);                   //сохранить параметры из переменных во флэш-память
void check_params( void);                       //проверить валидность (выставить default) настроек
void print_params(int nMask);                   //распечатать настройки

//Masks for save_params
#define SAVE_MASK_ALL 0x0F
#define SAVE_MASK_DI  0x01
#define SAVE_MASK_DO  0x02
#define SAVE_MASK_AI  0x04
#define SAVE_MASK_AO  0x08

//Masks for print_params
#define PRINT_MASK_ALL 0x0F
#define PRINT_MASK_DI  0x01
#define PRINT_MASK_DO  0x02
#define PRINT_MASK_AI  0x04
#define PRINT_MASK_AO  0x08

//размер страницы
#define PAGE_OFFSET 0x0200

//DI=BASE, DO=BASE+1page, AI=BASE+2page, AO=BASE+3page

// ADRESSES FOR FLASH-STORED PARAMS. PAGE 1 (GPI DI)
#define ADDR_PAGE1            0xF000
#define ADDR_DI               0xF000
#define ADDR_DI_CS_USAGE      0xF002
//#define OFFSET_ADDR_DI               0x00
//#define OFFSET_ADDR_DI_CS_USAGE      0x02

// ADRESSES FOR FLASH-STORED PARAMS. PAGE 2 (GPO DO)
#define ADDR_PAGE2              0xF200
#define ADDR_DO                 0xF200
#define ADDR_DO_CS_USAGE        0xF202
#define ADDR_DO_STARTUP_VALUESL 0xF204
#define ADDR_DO_STARTUP_VALUESH 0xF206
//#define OFFSET_ADDR_DO               0x00
//#define OFFSET_ADDR_DO_CS_USAGE      0x02
//#define OFFSET_ADDR_DO_STARTUP_VALSL 0x04
//#define OFFSET_ADDR_DO_STARTUP_VALSH 0x06

// ADRESSES FOR FLASH-STORED PARAMS. PAGE 3 (ADC AI)
#define ADDR_PAGE3            0xF400
#define ADDR_AI               0xF400
#define ADDR_AI_CS_USAGE      0xF402
#define ADDR_AI_COEFF_A       0xF404
//#define OFFSET_ADDR_AI               0x00
//#define OFFSET_ADDR_AI_CS_USAGE      0x02

// ADRESSES FOR FLASH-STORED PARAMS. PAGE 4 (DAC AO)
#define ADDR_PAGE4            0xF600
#define ADDR_AO               0xF600
#define ADDR_AO_CS_USAGE      0xF602
#define ADDR_AO_STARTUP_C0    0xF604
#define ADDR_AO_STARTUP_C1    0xF606
#define ADDR_AO_STARTUP_C2    0xF608
#define ADDR_AO_STARTUP_C3    0xF60A
#define ADDR_AO_STARTUP_C4    0xF60C
#define ADDR_AO_STARTUP_C5    0xF60E
#define ADDR_AO_COEFF_A       0xF610
#define ADDR_AO_COEFF_B       0xF612
//#define OFFSET_ADDR_AO               0x00
//#define OFFSET_ADDR_AO_CS_USAGE      0x02
//#define OFFSET_ADDR_AO_STARTUP_C0    0x04
//#define OFFSET_ADDR_AO_STARTUP_C1    0x06
//#define OFFSET_ADDR_AO_STARTUP_C2    0x08
//#define OFFSET_ADDR_AO_STARTUP_C3    0x0A
//#define OFFSET_ADDR_AO_STARTUP_C4    0x0C
//#define OFFSET_ADDR_AO_STARTUP_C5    0x0E

#endif
