#include <ADuC7026.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "logger.h"
#include "cb.h"
#include "checksumm.h"
#include "settings.h"
#include "leds.h"

extern unsigned char gl_cAddrAnalogueInput;     //адрес блока аналоговых входов   (АЦП)        TI  AI
extern unsigned char gl_cCheckSummUsageAI;      //использование CheckSumm
extern double        gl_dblAI_calA;             //A-коэффициент калибровки

extern char *gl_cpCircleBuffer;                 //кольцевой буфер байт входящих команд
extern int  gl_nCircleBufferPut;                //индекс где мы можем положить следующий входящий байт
extern int  gl_nCircleBufferGet;                //индекс указывающий последний взятый символ
extern char gl_bCircleBufferSettings;           //флаги кольцевого буфера
extern char gl_bIncomingCommand;                //Флаг принятой входящей команды

extern signed short *gl_sshp_adc;               //указатель на массив текущих значений каналов АЦП

//обработка команд АЦП
void ProcessAICommand( int nFirstCommandLen) {
  char strResponse[256];
  int nChannel;
  int i;

  char cByte;
  char acValue[8] = { 0, 0, 0, 0, 0, 0, 0, 0};
  float fValue;

  memset( strResponse, 0, 256);

  LogDebug( "AI: in with %d", nFirstCommandLen);

  //INCOMING CHECKSUM VERIFICATION
  if( gl_cCheckSummUsageAI && !ValidateCheckSumm( nFirstCommandLen)) {
    LedOn( LED_ERR);
    LogWarn( "AI: Incoming CheckSumm verification fail!");
    return;
  }
  
  /*
  RS485_SetToTransmit();
  for( i=0; i<10; i++) {
    printf( "DEBUG: %02d  --> %c\n", i, gl_cpCircleBuffer[ gl_nCircleBufferGet + i]);
  }
  RS485_SetToReceive();
  */


  if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '#' ) {
    // *************************************************************************
    // #-команды

    if( (  gl_cCheckSummUsageAI && nFirstCommandLen == 6) ||
        ( !gl_cCheckSummUsageAI && nFirstCommandLen == 4) ) {
      // *************************************************************************
      // #AA[CS]<cr>
      LogDebug( "AI: Recognized #{AA}[CS]<cr>: Get Analog Data In (all channels) command");
      //COMMAND.ACCEPTED
      LedOn( LED_RX);

      //COMMAND.ACTION

      //RESPONSE.PREPARATION
      strResponse[0] = '>';

      for( i=0; i<12; i++) {
        fValue = fabs( ( float) gl_sshp_adc[i] / 4096. * 3.0 * gl_dblAI_calA);

        memset( acValue, 0, 8);
        sprintf( acValue, "%.03f", fValue);

        strcat( strResponse, (( fValue < 0) ? "-":"+"));
        strcat( strResponse, acValue);
      }

      if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

      //RESPONSE.OUT
      LedOn( LED_TX);
      RS485_SetToTransmit();
      printf("%s\r", strResponse);
      RS485_SetToReceive();

      return;
    }
    else

    if( ( gl_cCheckSummUsageAI  && nFirstCommandLen == 7) ||
        ( !gl_cCheckSummUsageAI && nFirstCommandLen == 5) ) {
      // *************************************************************************
      // #AAN[CS]<cr>
      LogDebug( "AI: Recognized #{AA}N[CS]<cr>: Get Analog Data In (current channel) command");

      //PARAM.ANALYZE
      cByte = gl_cpCircleBuffer[ cb_getInd_inc(4)];
      nChannel = -1;
      switch( cByte) {
        case '0': nChannel = 0; break;
        case '1': nChannel = 1; break;
        case '2': nChannel = 2; break;
        case '3': nChannel = 3; break;
        case '4': nChannel = 4; break;
        case '5': nChannel = 5; break;
        case '6': nChannel = 6; break;
        case '7': nChannel = 7; break;
        case '8': nChannel = 8; break;
        case '9': nChannel = 9; break;
        case 'A': nChannel = 10; break;
        case 'B': nChannel = 11; break;
      }

      if( nChannel != -1) {
        LogDebug( "AI: Get Analog Data In command for channel #%d  (#AAN...)", nChannel);
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        fValue = fabs( ( float) gl_sshp_adc[nChannel] / 4096. * 3.0 * gl_dblAI_calA);
        sprintf( acValue, "%.03f", fValue);

        strResponse[0] = '>';
        strcat( strResponse, ( fValue < 0) ? "-":"+");
        strcat( strResponse, acValue);

        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();
        return;

      }
      else {
        LogDebug( "AI: Bad channel value '%c'", cByte);
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);
        
        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);
        
        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    else

    if( (  gl_cCheckSummUsageAI && nFirstCommandLen == 12) ||
        ( !gl_cCheckSummUsageAI && nFirstCommandLen == 10) ) {
      // *************************************************************************
      // #{AA}A{VALUE}
      LogDebug( "AI: Recognized #{AA}A{VALUE}<cr>: Set A-calibration coefficient command");

      //PARAM.ANALYZE
      for( i=0; i<5; acValue[ i++] = gl_cpCircleBuffer[ cb_getInd_inc( 5 + i)]);
      fValue = atof( acValue);
      if( fValue >= 0. && fValue <= 10.) {
        LogDebug( "AI: #{AA}A{VALUE}: value '%.3f'", fValue);
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //COMMAND.ACTION
        gl_dblAI_calA = fValue;
        save_params( SAVE_MASK_AI);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];  //A
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];  //A
        strResponse[ 3] = gl_cpCircleBuffer[ cb_getInd_inc( 4)];  //A
        strResponse[ 4] = gl_cpCircleBuffer[ cb_getInd_inc( 5)];  //1
        strResponse[ 5] = gl_cpCircleBuffer[ cb_getInd_inc( 6)];  //.
        strResponse[ 6] = gl_cpCircleBuffer[ cb_getInd_inc( 7)];  //0
        strResponse[ 7] = gl_cpCircleBuffer[ cb_getInd_inc( 8)];  //0
        strResponse[ 8] = gl_cpCircleBuffer[ cb_getInd_inc( 9)];  //0

        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogWarn( "AO: #{AA}A{VALUE}: Bad value '%s'", acValue);
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    
    else {
      LogWarn( "AI: Unknown #command");
      //COMMAND.NOT.ACCEPTED
      LedOn( LED_ERR);

      //RESPONSE.PREPARATION
      sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
      if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

      //RESPONSE.OUT
      RS485_SetToTransmit();
      printf("%s\r", strResponse);
      RS485_SetToReceive();

      return;
    }

  }
  else if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '$' ) {
    // *************************************************************************
    // $-команды

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '2') {
      // *************************************************************************
      // $AA2

      if( ( gl_cCheckSummUsageAI  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageAI && nFirstCommandLen == 5)) {

        LogDebug( "AI: Recognized ${AA}2<cr>: Config request command");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

            
        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';

        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];    //AA
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];    //AA

        strResponse[ 3] = '0';                                      //TT
        strResponse[ 4] = '0';                                      //TT

        strResponse[ 5] = '0';                                      //CC
        strResponse[ 6] = '0';                                      //CC

        strResponse[ 7] = gl_cCheckSummUsageAI ? '4':'0';           //FF
        strResponse[ 8] = '0';                                      //FF

        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);


        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();
        
        return;
      }
      else {
        LogInfo( "AI: Wrong formatted $AA2 command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    else

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '8') {
      // *************************************************************************
      // $AA8Ci[CS]<cr>

      if( ( gl_cCheckSummUsageAI  && nFirstCommandLen == 9) ||
          ( !gl_cCheckSummUsageAI && nFirstCommandLen == 7)) {

        LogDebug( "AI: Recognized ${AA}8Ci<cr>: Incoming voltage range request command");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];
        strResponse[ 3] = '8';
        strResponse[ 4] = 'C';
        strResponse[ 5] = gl_cpCircleBuffer[ cb_getInd_inc( 6)];
        strResponse[ 6] = 'R';
        strResponse[ 7] = '0';
        strResponse[ 8] = '9';

        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "AI: Wrong formatted $AA8Ci command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);
        
        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    else

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'A') {
      // *************************************************************************
      // ${AA}A[CS]<cr>
      
      
      if( ( gl_cCheckSummUsageAI  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageAI && nFirstCommandLen == 5)) {

        LogDebug( "AI: Recognized ${AA}A[CS]<cr>: Request A-calib-coeff command");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];
        sprintf( &strResponse[3], "A%.03f", gl_dblAI_calA);

        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "AI: Wrong formatted ${AA}A command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);
        
        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'M') {
      // *************************************************************************
      // $AAM
      if( ( gl_cCheckSummUsageAI  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageAI && nFirstCommandLen == 5)) {

        LogDebug( "AI: Recognized ${AA}M[CS]<cr>: Get module name command");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];
        strResponse[ 3] = 'A';
        strResponse[ 4] = 'D';
        strResponse[ 5] = 'C';

        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "AI: Wrong formatted $AAM command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
        if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    else
    {
      LogDebug( "AI: Unknown $command.");
      //COMMAND.NOT.ACCEPTED
      LedOn( LED_ERR);

      //RESPONSE.PREPARATION
      sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
      if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

      //RESPONSE.OUT
      RS485_SetToTransmit();
      printf("%s\r", strResponse);
      RS485_SetToReceive();

      return;
    }
  }

  LogDebug( "AI: Unknown command (neither $ nor # at the beginning).");
  //COMMAND.NOT.ACCEPTED
  LedOn( LED_ERR);

  //RESPONSE.PREPARATION
  sprintf( strResponse, "?%02X", gl_cAddrAnalogueInput);
  if( gl_cCheckSummUsageAI) AddCheckSumm( strResponse);

  //RESPONSE.OUT
  RS485_SetToTransmit();
  printf("%s\r", strResponse);
  RS485_SetToReceive();
}
