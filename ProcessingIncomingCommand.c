#include <ADuC7026.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "version.h"
#include "settings.h"
#include "debug.h"
#include "logger.h"
//#include "serial.h"
#include "cb.h"
#include "leds.h"
#include "ProcessingAICommands.h"
#include "ProcessingAOCommands.h"
#include "ProcessingDICommands.h"
#include "ProcessingDOCommands.h"


extern int gl_nLogLevel;                        //уровень логирования

extern char gl_c_EmergencyCode;                 //Device error code


extern unsigned char gl_cAddrDigitalInput;      //адрес блока цифровых входов     (реле вход)  TS  DI
extern unsigned char gl_cCheckSummUsageDI;      //использование CheckSumm

extern unsigned char gl_cAddrDigitalOutput;     //адрес блока цифровых выходов    (реле выход) TU  DO
extern unsigned char gl_cCheckSummUsageDO;      //использование CheckSumm

extern unsigned char gl_cAddrAnalogueInput;     //адрес блока аналоговых входов   (АЦП)        TI  AI
extern unsigned char gl_cCheckSummUsageAI;      //использование CheckSumm

extern unsigned char gl_cAddrAnalogueOutput;    //адрес блока аналоговых выходов  (ЦАП)            AO
extern unsigned char gl_cCheckSummUsageAO;      //использование CheckSumm


extern char *gl_cpCircleBuffer;                 //кольцевой буфер байт входящих команд
extern int  gl_nCircleBufferPut;                //индекс где мы можем положить следующий входящий байт
extern int  gl_nCircleBufferGet;                //индекс указывающий последний взятый символ
extern char gl_bCircleBufferSettings;           //флаги кольцевого буфера
extern char gl_bIncomingCommand;                //Флаг принятой входящей команды

//начальная точка обработки команд
void processIncomingCommand( void) {
  int nLen;
  char caAddress[3] = { 0, 0, 0};
  char caNewAddress[3] = { 0, 0, 0};
  int nAddress;
  int nNewAddress;
  int nFirstCmdLastIndx;
  int nFirstCmdLen = 0;
  char cOk;

  //GP2DAT |= 1 << (16 + 3);      //TempLED on     	 (p2.3) = 1

  LogDebug( "PIC: in: CB_IC: %d  CB_GET: %d  CB_PUT: %d", gl_bIncomingCommand, gl_nCircleBufferGet, gl_nCircleBufferPut);

  do {
    nFirstCmdLen++;
    nFirstCmdLastIndx = ( gl_nCircleBufferGet + nFirstCmdLen) % IN_COMMAND_BUF_LEN;

    if( nFirstCmdLastIndx == gl_nCircleBufferPut) {
      //мы прокрутили буфер вперёд, но не нашли там 0x0D
      //СТРАННО
      //такое может быть если мы прислали команду, а после неё какие-то байтики не завершённые 0x0D
      //команда выполнится, флаг что ещё что-то есть в буфере не опустится и мы попадём сюда
      LogWarn( "PIC: No trailing 0x0D! STRANGE!");
      gl_bIncomingCommand = 0;
      //GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
      return;
    }

    if( gl_cpCircleBuffer[ nFirstCmdLastIndx] == 0x0D) {
      //нашли первый 0x0D
      break;
    }

  } while(1);
  LogDebug( "PIC: First command in buffer is %d bytes len", nFirstCmdLen);

  //определим сколько всего байт в буфере
  nLen = ( gl_nCircleBufferPut + IN_COMMAND_BUF_LEN - gl_nCircleBufferGet) % IN_COMMAND_BUF_LEN;
  nLen--;
  LogDebug( "PIC: First command is %d bytes len", nFirstCmdLen);
  LogDebug( "PIC: Total data in buffer is %d bytes len", nLen);

  if( nFirstCmdLen > 2) {

    //У нас точно есть 2 байта - это должен быть адрес к кому обращаются
    caAddress[0] = gl_cpCircleBuffer[ cb_getInd_inc(2)];
    caAddress[1] = gl_cpCircleBuffer[ cb_getInd_inc(3)];
    
    if( isdigit( caAddress[0]) && isdigit( caAddress[1]))
      nAddress = strtol( caAddress, NULL, 16);
    else
      nAddress = -1;

    if( cb_getInd_inc(1) != '@')
      //выведем его, если это не common-команда
      LogDebug( "PIC: Address: %d", nAddress);

    //*** *** *** *** *** *** 
    //COMMON (FOR ALL MODULES) COMMANDS

    //Configure command
    if( nFirstCmdLen == 12) {
      if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '%') {
        //%{AA}{NN}{TT}{CC}{FF}<cr>

        LogDebug( "PIC: Recognized module configure command ((pct){AA}{NN}{TT}{CC}{FF}<cr>)");
        LedOn( LED_RX);

        if( nAddress == gl_cAddrDigitalInput) {
          //*** *** *** *** *** *** 
          LogDebug( "PIC: Configuring GPI-module");

          caNewAddress[0] = gl_cpCircleBuffer[ cb_getInd_inc(4)];
          caNewAddress[1] = gl_cpCircleBuffer[ cb_getInd_inc(5)];
          nNewAddress = strtol( caNewAddress, NULL, 16);
          LogDebug( "PIC: New address: %d", nNewAddress);

          cOk = 1;
               if( nNewAddress == gl_cAddrDigitalOutput)  cOk = 0;
          else if( nNewAddress == gl_cAddrAnalogueInput)  cOk = 0;
          else if( nNewAddress == gl_cAddrAnalogueOutput) cOk = 0;

          if( cOk == 1) {
            gl_cAddrDigitalInput = nNewAddress;
            if( gl_cpCircleBuffer[ cb_getInd_inc(10)] == '4') {
              LogDebug( "PIC: Turning checksumm usage on");
              gl_cCheckSummUsageDI = 1;
            }
            else {
              LogDebug( "PIC: Turning checksumm usage off");
              gl_cCheckSummUsageDI = 0;
            }
            save_params( SAVE_MASK_DI);

            LogDebug( "PIC: Command processed!");
            LedOn( LED_TX);
            RS485_SetToTransmit();
            putchar( '!');
          }
          else {
            LedOn( LED_ERR);
            LogDebug( "PIC: Bad command!");
            RS485_SetToTransmit();
            putchar( '?');
          }

          putchar( caAddress[0]);
          putchar( caAddress[1]);
          putchar( 0x0D);
          RS485_SetToReceive();
        }
        else if( nAddress == gl_cAddrDigitalOutput) {
          //*** *** *** *** *** *** 
          LogDebug( "Configuring GPO-module");

          caNewAddress[0] = gl_cpCircleBuffer[ cb_getInd_inc(4)];
          caNewAddress[1] = gl_cpCircleBuffer[ cb_getInd_inc(5)];
          nNewAddress = strtol( caNewAddress, NULL, 16);
          LogDebug( "PIC: New address: %d", nNewAddress);

          cOk = 1;
               if( nNewAddress == gl_cAddrDigitalInput)   cOk = 0;
          else if( nNewAddress == gl_cAddrAnalogueInput)  cOk = 0;
          else if( nNewAddress == gl_cAddrAnalogueOutput) cOk = 0;

          if( cOk == 1) {
            gl_cAddrDigitalOutput = nNewAddress;
            if( gl_cpCircleBuffer[ cb_getInd_inc(10)] == '4') {
              LogDebug( "PIC: Turning checksumm usage on");
              gl_cCheckSummUsageDO = 1;
            }
            else {
              LogDebug( "PIC: Turning checksumm usage off");
              gl_cCheckSummUsageDO = 0;
            }
            save_params( SAVE_MASK_DO);

            LogDebug( "PIC: Command processed!");
            LedOn( LED_TX);
            RS485_SetToTransmit();
            putchar( '!');
          }
          else {
            LogDebug( "PIC: Bad command!");
            LedOn( LED_ERR);
            RS485_SetToTransmit();
            putchar( '?');
          }

          putchar( caAddress[0]);
          putchar( caAddress[1]);
          putchar( 0x0D);
          RS485_SetToReceive();
        }
        else if( nAddress == gl_cAddrAnalogueInput) {
          //*** *** *** *** *** *** 
          LogDebug( "Configuring ADC-module");

          caNewAddress[0] = gl_cpCircleBuffer[ cb_getInd_inc(4)];
          caNewAddress[1] = gl_cpCircleBuffer[ cb_getInd_inc(5)];
          nNewAddress = strtol( caNewAddress, NULL, 16);
          LogDebug( "PIC: New address: %d", nNewAddress);

          cOk = 1;
               if( nNewAddress == gl_cAddrDigitalInput)   cOk = 0;
          else if( nNewAddress == gl_cAddrDigitalOutput)  cOk = 0;
          else if( nNewAddress == gl_cAddrAnalogueOutput) cOk = 0;

          if( cOk == 1) {
            gl_cAddrAnalogueInput = nNewAddress;
            if( gl_cpCircleBuffer[ cb_getInd_inc(10)] == '4') {
              LogDebug( "PIC: Turning checksumm usage on");
              gl_cCheckSummUsageAI = 1;
            }
            else {
              LogDebug( "PIC: Turning checksumm usage off");
              gl_cCheckSummUsageAI = 0;
            }

            save_params( SAVE_MASK_AI);

            LogDebug( "PIC: Command processed!");
            LedOn( LED_TX);
            RS485_SetToTransmit();
            putchar( '!');
          }
          else {
            LogDebug( "PIC: Bad command!");
            LedOn( LED_ERR);
            RS485_SetToTransmit();
            putchar( '?');
          }

          putchar( caAddress[0]);
          putchar( caAddress[1]);
          putchar( 0x0D);
          RS485_SetToReceive();
        }
        else if( nAddress == gl_cAddrAnalogueOutput) {
          //*** *** *** *** *** ***
          LogDebug( "Configuring DAC-module");

          caNewAddress[0] = gl_cpCircleBuffer[ cb_getInd_inc(4)];
          caNewAddress[1] = gl_cpCircleBuffer[ cb_getInd_inc(5)];
          nNewAddress = strtol( caNewAddress, NULL, 16);
          LogDebug( "PIC: New address: %d", nNewAddress);

          cOk = 1;
               if( nNewAddress == gl_cAddrDigitalInput)   cOk = 0;
          else if( nNewAddress == gl_cAddrDigitalOutput)  cOk = 0;
          else if( nNewAddress == gl_cAddrAnalogueInput) cOk = 0;

          if( cOk == 1) {
            gl_cAddrAnalogueOutput = nNewAddress;
            if( gl_cpCircleBuffer[ cb_getInd_inc(10)] == '4') {
              LogDebug( "PIC: Turning checksumm usage on");
              gl_cCheckSummUsageAO = 1;
            }
            else {
              LogDebug( "PIC: Turning checksumm usage off");
              gl_cCheckSummUsageAO = 0;
            }

            save_params( SAVE_MASK_AO);

            LogDebug( "PIC: Command processed!");
            LedOn( LED_TX);
            RS485_SetToTransmit();
            putchar( '!');
          }
          else {
            LogDebug( "PIC: Bad command!");
            LedOn( LED_ERR);
            RS485_SetToTransmit();
            putchar( '?');
          }

          putchar( caAddress[0]);
          putchar( caAddress[1]);
          putchar( 0x0D);
          RS485_SetToReceive();

          /*
          RS485_SetToTransmit();
          for( i=0; i<10; i++) {
            printf( "DEBUG: %02d  --> %c\n", i, gl_cpCircleBuffer[ gl_nCircleBufferGet + i]);
          }
          RS485_SetToReceive();
          */
        }
        else {
          LedOn( LED_ERR);
          RS485_SetToTransmit();
          printf( "?%02X", nAddress);
          putchar_nocheck( 0x0D);
          RS485_SetToReceive();
        }

        //забираем из буфера 12 байт команды (% A A N N T T C C F F <cr>)
        gl_nCircleBufferGet = ( gl_nCircleBufferGet + 12) % IN_COMMAND_BUF_LEN;

        //опускаем флаг готовой входящей команды
        if( nLen == 12)
          gl_bIncomingCommand = 0;

        //GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
        return;
      }
    }
    else
    // ********************************************************************************************************
    //6  BYTES  INCOMING COMMAND
    if( nFirstCmdLen == 6) {
      if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '@' &&
            gl_cpCircleBuffer[ cb_getInd_inc(2)] == 'L' && gl_cpCircleBuffer[ cb_getInd_inc(3)] == 'O') {

        LogDebug( "PIC: Recognized set loglevel command (@LOGX<cr>)");
        LedOn( LED_RX);

        nAddress = gl_cpCircleBuffer[ cb_getInd_inc(5)] - '0';
        if( nAddress >=0 && nAddress <=6) {
          gl_nLogLevel = nAddress;
          LedOn( LED_TX);
          RS485_SetToTransmit();
          printf( "!LOG%c", gl_cpCircleBuffer[ cb_getInd_inc(5)]);
          putchar_nocheck( 0x0D);
          RS485_SetToReceive();
        }
        else {
          LedOn( LED_ERR);
          RS485_SetToTransmit();
          printf( "?LOG%c", gl_cpCircleBuffer[ cb_getInd_inc(5)]);
          putchar_nocheck( 0x0D);
          RS485_SetToReceive();
        }

        //забираем из буфера 6 байт команды (@ L O G X <cr>)
        gl_nCircleBufferGet = ( gl_nCircleBufferGet + 6) % IN_COMMAND_BUF_LEN;

        //опускаем флаг готовой входящей команды
        if( nLen == 6)
          gl_bIncomingCommand = 0;
        
        //GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
        return; 
      }

    }
    // ********************************************************************************************************
    //4  BYTES  INCOMING COMMAND
    if( nFirstCmdLen == 4) {
      if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '@' &&
            gl_cpCircleBuffer[ cb_getInd_inc(2)] == 'C' && gl_cpCircleBuffer[ cb_getInd_inc(3)] == 'S') {

        LogDebug( "PIC: Recognized checksumm usage request command (@CS<cr>)");
        LedOn( LED_RX);

        LedOn( LED_TX);
        RS485_SetToTransmit();

        printf( "!AI:%c AO:%c DI:%c DO:%c",
              '0' + gl_cCheckSummUsageAI,
              '0' + gl_cCheckSummUsageAO,
              '0' + gl_cCheckSummUsageDI,
              '0' + gl_cCheckSummUsageDO);
        putchar_nocheck( 0x0D);
        RS485_SetToReceive();
              
        //забираем из буфера 4 байта команды (@ C S <cr>)
        gl_nCircleBufferGet = ( gl_nCircleBufferGet + 4) % IN_COMMAND_BUF_LEN;

        //опускаем флаг готовой входящей команды
        if( nLen == 4)
          gl_bIncomingCommand = 0;

        //GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
        return;
      }
    }
    else

    //3  BYTES  INCOMING COMMAND
    if( nFirstCmdLen == 3) {

      //@A<cr>
      if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '@' && gl_cpCircleBuffer[ cb_getInd_inc(2)] == 'A') {
        LogDebug( "PIC: Recognized adresses request command (@A<cr>)");
        LedOn( LED_RX);

        LedOn( LED_TX);
        RS485_SetToTransmit();
        
        printf( "!AI:0x%02X AO:0x%02X DI:0x%02X DO:0x%02X",
            gl_cAddrAnalogueInput,
            gl_cAddrAnalogueOutput,
            gl_cAddrDigitalInput,
            gl_cAddrDigitalOutput);

        putchar_nocheck( 0x0D);
        RS485_SetToReceive();
        
        //забираем из буфера 3 байта команды (@ A <cr>)
        gl_nCircleBufferGet = ( gl_nCircleBufferGet + 3) % IN_COMMAND_BUF_LEN;

        //опускаем флаг готовой входящей команды
        if( nLen == 3)
          gl_bIncomingCommand = 0;

        //GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
        return;
      }
      else
        
      //@F<cr>
      if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '@' && gl_cpCircleBuffer[ cb_getInd_inc(2)] == 'F') {

        LogDebug( "PIC: Recognized firmware version request command (@F<cr>)");
        LedOn( LED_RX);

        LedOn( LED_TX);
        RS485_SetToTransmit();
        putchar( '!');
        //putchar( gl_cpCircleBuffer[ cb_getInd_inc(2)]);
        //putchar( gl_cpCircleBuffer[ cb_getInd_inc(3)]);
        if( VERSION_MAJOR < 10) {
          putchar( '0');
          putchar( '0' + VERSION_MAJOR);
        } else {
          putchar( '0' + VERSION_MAJOR / 10);
          putchar( '0' + VERSION_MAJOR % 10);
        }

        if( VERSION_MIDDLE < 10) {
          putchar( '0');
          putchar( '0' + VERSION_MIDDLE);
        } else {
          putchar( '0' + VERSION_MIDDLE / 10);
          putchar( '0' + VERSION_MIDDLE % 10);
        }

        if( VERSION_MINOR < 10) {
          putchar( '0');
          putchar( '0' + VERSION_MINOR);
        } else {
          putchar( '0' + VERSION_MINOR / 10);
          putchar( '0' + VERSION_MINOR % 10);
        }
        
        putchar( 0x0D);
        RS485_SetToReceive();


        //забираем из буфера 3 байта команды (@ F <cr>)
        gl_nCircleBufferGet = ( gl_nCircleBufferGet + 3) % IN_COMMAND_BUF_LEN;

        //опускаем флаг готовой входящей команды
        if( nLen == 3)
          gl_bIncomingCommand = 0;

        //GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
        return;
      }
      else
        
      //@S<cr>
      if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '@' && gl_cpCircleBuffer[ cb_getInd_inc(2)] == 'S') {

        LogDebug( "PIC: Recognized settings request command (@S<cr>)");
        LedOn( LED_RX);
        print_params( PRINT_MASK_ALL);


        //забираем из буфера 3 байта команды (@ F <cr>)
        gl_nCircleBufferGet = ( gl_nCircleBufferGet + 3) % IN_COMMAND_BUF_LEN;

        //опускаем флаг готовой входящей команды
        if( nLen == 3)
          gl_bIncomingCommand = 0;

        //GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
        return;
      }

      
    }

    //*** *** *** *** *** *** 
    //SPECIFIC (FOR CURRENT MODULE) COMMANDS DISPATCHER
    if( nAddress == gl_cAddrAnalogueInput) {
      LogInfo( "PIC: Command for AI!");
      ProcessAICommand( nFirstCmdLen);
    }
    else if( nAddress == gl_cAddrAnalogueOutput) {
      LogInfo( "PIC: Command for AO!");
      ProcessAOCommand( nFirstCmdLen);
    }
    else if( nAddress == gl_cAddrDigitalInput) {
      LogInfo( "PIC: Command for DI!");
      ProcessDICommand( nFirstCmdLen);
    }
    else if( nAddress == gl_cAddrDigitalOutput) {
      LogInfo( "PIC: Command for DO!");
      ProcessDOCommand( nFirstCmdLen);
    }
    else {
      LogWarn( "PIC: Not my address! (%d)", nAddress);

      /*
      RS485_SetToTransmit();
      printf( "?%02X", gl_cAddrAnalogueInput);
      putchar_nocheck( 0x0D);
      RS485_SetToReceive();
      */
    }

  }
  else {
    //мы отсекли случай когда команда <3 байт
    LogDebug( "PIC: Incoming command is too short!");
  }


  gl_nCircleBufferGet = nFirstCmdLastIndx;
  if( nFirstCmdLen == nLen) {
    //мы выскребли все команды - в буфере больше байт нет
    //соответственно сбрасываем флаг готовности команды в буфере
    gl_bIncomingCommand = 0;
  }
  
  LogDebug( "PIC: out: CB_IC: %d  CB_GET: %d  CB_PUT: %d", gl_bIncomingCommand, gl_nCircleBufferGet, gl_nCircleBufferPut);
  
}
