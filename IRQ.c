/******************************************************************************/
/* IRQ.C: IRQ Handler for Dimmer Example                                      */
/******************************************************************************/
/* This file is part of the uVision/ARM development tools.                    */
/* Copyright (c) 2005-2006 Keil Software. All rights reserved.                */
/* This software may only be used under the terms of a valid, current,        */
/* end user licence from KEIL for a compatible version of KEIL software       */
/* development tools. Nothing else gives you the right to use this software.  */
/******************************************************************************/

#include <ADuC7026.H>               // ADuC7026 definitions
#include "cb.h"

extern char gl_bCircleBufferSettings;	//флаги кольцевого буфера:
extern int  gl_nCircleBufferPut;      //индекс где мы можем положить следующий входящий байт
extern int  gl_nCircleBufferGet;      //индекс указывающий последний взятый символ
extern char gl_caCircleBuffer[];			
extern char gl_bIncomingCommand;

volatile int T0_Tick;               	// Timer 0 Current Tick Value


