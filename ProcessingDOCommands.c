#include <ADuC7026.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "settings.h"
#include "logger.h"
#include "cb.h"
#include "checksumm.h"
#include "leds.h"

extern unsigned char gl_cAddrDigitalOutput;     //адрес блока цифровых выходов    (реле выход) TU  DO
extern unsigned char gl_cCheckSummUsageDO;      //использование CheckSumm
extern unsigned short gl_ushStartupValueDoL;    //стартовые значения каналов цифровых выходов (младшие 16 бит = каналов)
extern unsigned short gl_ushStartupValueDoH;    //стартовые значения каналов цифровых выходов (старшие 16 бит = каналов)

extern char *gl_cpCircleBuffer;                 //кольцевой буфер байт входящих команд
extern int  gl_nCircleBufferPut;                //индекс где мы можем положить следующий входящий байт
extern int  gl_nCircleBufferGet;                //индекс указывающий последний взятый символ
extern char gl_bCircleBufferSettings;           //флаги кольцевого буфера
extern char gl_bIncomingCommand;                //Флаг принятой входящей команды


//обработка команд цифровых выходов
void ProcessDOCommand( int nFirstCommandLen) {
  char strResponse[256];
  
  unsigned char acMask[7] = { 0, 0, 0, 0, 0, 0, 0};
  unsigned int unChannel;
  //unsigned char csByte;
  unsigned int un_GPOStates = 0;

  memset( strResponse, 0, 256);

  LogDebug( "DO: in with %d", nFirstCommandLen);

  //INCOMING CHECKSUM VERIFICATION
  if( gl_cCheckSummUsageDO && !ValidateCheckSumm( nFirstCommandLen)) {
    LedOn( LED_ERR);
    LogWarn( "DO: Incoming CheckSumm verification fail!");
    return;
  }
  
  if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '#' ) {
    // **************************************************************************************
    // #-Команды

    if( ( gl_cCheckSummUsageDO  && nFirstCommandLen == 13) ||
        ( !gl_cCheckSummUsageDO && nFirstCommandLen == 11)) {
      // **************************************************************************************
      // #{AA}{BBBBBB}{data}

      LogDebug( "DO: Recognized Digital Current Data Out set command (#{AA}{BBBBBB}{data})");
      //COMMAND.ACCEPTED
      LedOn( LED_RX);

      //COMMAND.ACTION
      strncpy( ( char *) acMask, &gl_cpCircleBuffer[ cb_getInd_inc(4)], 6);
      unChannel = strtol( ( char *) acMask, NULL, 16);

      LogDebug( "DO: #{AA}{BBBBBB}{data}: channel=%d (0x%06x)", unChannel, unChannel);
      LogDebug( "DO: #{AA}{BBBBBB}{data}: gl_cpCircleBuffer[ cb_getInd_inc(10)]='%c'", gl_cpCircleBuffer[ cb_getInd_inc(10)]);

      /*
      LogDebug( "DO: #AABBBBBB(data): GP0DAT=0x%08x", GP0DAT);
      LogDebug( "DO: #AABBBBBB(data): GP0SET=0x%08x", GP0SET);
      LogDebug( "DO: #AABBBBBB(data): GP0CLR=0x%08x", GP0CLR);
      LogDebug( "DO: #AABBBBBB(data): GP1DAT=0x%08x", GP1DAT);
      LogDebug( "DO: #AABBBBBB(data): GP1SET=0x%08x", GP1SET);
      LogDebug( "DO: #AABBBBBB(data): GP1CLR=0x%08x", GP1CLR);
      LogDebug( "DO: #AABBBBBB(data): GP2DAT=0x%08x", GP2DAT);
      LogDebug( "DO: #AABBBBBB(data): GP2SET=0x%08x", GP2SET);
      LogDebug( "DO: #AABBBBBB(data): GP2CLR=0x%08x", GP2CLR);
      LogDebug( "DO: #AABBBBBB(data): GP3DAT=0x%08x", GP3DAT);
      LogDebug( "DO: #AABBBBBB(data): GP3SET=0x%08x", GP3SET);
      LogDebug( "DO: #AABBBBBB(data): GP3CLR=0x%08x", GP3CLR);
      LogDebug( "DO: #AABBBBBB(data): GP4DAT=0x%08x", GP4DAT);
      LogDebug( "DO: #AABBBBBB(data): GP4SET=0x%08x", GP4SET);
      LogDebug( "DO: #AABBBBBB(data): GP4CLR=0x%08x", GP4CLR);
      */
      
      if( gl_cpCircleBuffer[ cb_getInd_inc(10)] == '0') {
        //drop bit
        if( unChannel & 0x000001) GP2CLR = 0x080000;   // TU1 = P2.3
        if( unChannel & 0x000002) GP4CLR = 0x400000;   // TU2 = P4.6
        if( unChannel & 0x000004) GP4CLR = 0x800000;   // TU3 = P4.7
        if( unChannel & 0x000008) GP0CLR = 0x400000;   // TU4 = P0.6

        if( unChannel & 0x000010) GP0CLR = 0x040000;   // TU5 = P0.2
        if( unChannel & 0x000020) GP3CLR = 0x010000;   // TU6 = P3.0
        if( unChannel & 0x000040) GP3CLR = 0x020000;   // TU7 = P3.1
        if( unChannel & 0x000080) GP3CLR = 0x040000;   // TU8 = P3.2


        if( unChannel & 0x000100) GP3CLR = 0x080000;   // TU9 = P3.3
        if( unChannel & 0x000200) GP2CLR = 0x100000;   //TU10 = P2.4
        if( unChannel & 0x000400) GP0CLR = 0x080000;   //TU11 = P0.3
        if( unChannel & 0x000800) GP2CLR = 0x200000;   //TU12 = P2.5

        if( unChannel & 0x001000) GP2CLR = 0x400000;   //TU13 = P2.6
        if( unChannel & 0x002000) GP3CLR = 0x100000;   //TU14 = P3.4
        if( unChannel & 0x004000) GP3CLR = 0x200000;   //TU15 = P3.5
        if( unChannel & 0x008000) GP0CLR = 0x100000;   //TU16 = P0.4


        if( unChannel & 0x010000) GP0CLR = 0x200000;   //TU17 = P0.5
        if( unChannel & 0x020000) GP2CLR = 0x010000;   //TU18 = P2.0
        if( unChannel & 0x040000) GP0CLR = 0x800000;   //TU19 = P0.7
        if( unChannel & 0x080000) GP3CLR = 0x400000;   //TU20 = P3.6
      }
      else {
        //raise bit
        if( unChannel & 0x000001) GP2SET = 0x080000;   // TU1 = P2.3
        if( unChannel & 0x000002) GP4SET = 0x400000;   // TU2 = P4.6
        if( unChannel & 0x000004) GP4SET = 0x800000;   // TU3 = P4.7
        if( unChannel & 0x000008) GP0SET = 0x400000;   // TU4 = P0.6

        if( unChannel & 0x000010) GP0SET = 0x040000;   // TU5 = P0.2
        if( unChannel & 0x000020) GP3SET = 0x010000;   // TU6 = P3.0
        if( unChannel & 0x000040) GP3SET = 0x020000;   // TU7 = P3.1
        if( unChannel & 0x000080) GP3SET = 0x040000;   // TU8 = P3.2


        if( unChannel & 0x000100) GP3SET = 0x080000;   // TU9 = P3.3
        if( unChannel & 0x000200) GP2SET = 0x100000;   //TU10 = P2.4
        if( unChannel & 0x000400) GP0SET = 0x080000;   //TU11 = P0.3
        if( unChannel & 0x000800) GP2SET = 0x200000;   //TU12 = P2.5

        if( unChannel & 0x001000) GP2SET = 0x400000;   //TU13 = P2.6
        if( unChannel & 0x002000) GP3SET = 0x100000;   //TU14 = P3.4
        if( unChannel & 0x004000) GP3SET = 0x200000;   //TU15 = P3.5
        if( unChannel & 0x008000) GP0SET = 0x100000;   //TU16 = P0.4


        if( unChannel & 0x010000) GP0SET = 0x200000;   //TU17 = P0.5
        if( unChannel & 0x020000) GP2SET = 0x010000;   //TU18 = P2.0
        if( unChannel & 0x040000) GP0SET = 0x800000;   //TU19 = P0.7
        if( unChannel & 0x080000) GP3SET = 0x400000;   //TU20 = P3.6
      }


      /*
      if( unChannel & 0x000001) GP2SET = 0x080000; else GP2CLR = 0x080000;   // TU1 = P2.3
      if( unChannel & 0x000002) GP4SET = 0x400000; else GP4CLR = 0x400000;   // TU2 = P4.6
      if( unChannel & 0x000004) GP4SET = 0x800000; else GP4CLR = 0x800000;   // TU3 = P4.7
      if( unChannel & 0x000008) GP0SET = 0x400000; else GP0CLR = 0x400000;   // TU4 = P0.6

      if( unChannel & 0x000010) GP0SET = 0x040000; else GP0CLR = 0x040000;   // TU5 = P0.2
      if( unChannel & 0x000020) GP3SET = 0x010000; else GP3CLR = 0x010000;   // TU6 = P3.0
      if( unChannel & 0x000040) GP3SET = 0x020000; else GP3CLR = 0x020000;   // TU7 = P3.1
      if( unChannel & 0x000080) GP3SET = 0x040000; else GP3CLR = 0x040000;   // TU8 = P3.2


      if( unChannel & 0x000100) GP2SET = 0x080000; else GP3CLR = 0x080000;   // TU9 = P3.3
      if( unChannel & 0x000200) GP2SET = 0x100000; else GP2CLR = 0x100000;   //TU10 = P2.4
      if( unChannel & 0x000400) GP0SET = 0x080000; else GP0CLR = 0x080000;   //TU11 = P0.3
      if( unChannel & 0x000800) GP2SET = 0x200000; else GP2CLR = 0x200000;   //TU12 = P2.5

      if( unChannel & 0x001000) GP2SET = 0x400000; else GP2CLR = 0x400000;   //TU13 = P2.6
      if( unChannel & 0x002000) GP3SET = 0x100000; else GP3CLR = 0x100000;   //TU14 = P3.4
      if( unChannel & 0x004000) GP3SET = 0x200000; else GP3CLR = 0x200000;   //TU15 = P3.5
      if( unChannel & 0x008000) GP0SET = 0x100000; else GP0CLR = 0x100000;   //TU16 = P0.4


      if( unChannel & 0x010000) GP0SET = 0x200000; else GP0CLR = 0x200000;   //TU17 = P0.5
      if( unChannel & 0x020000) GP2SET = 0x010000; else GP2CLR = 0x010000;   //TU18 = P2.0
      if( unChannel & 0x040000) GP0SET = 0x800000; else GP0CLR = 0x800000;   //TU19 = P0.7
      if( unChannel & 0x080000) GP3SET = 0x400000; else GP3CLR = 0x400000;   //TU20 = P3.6
      */

      /*
      LogDebug( "DO: #AABBBBBB(data): GP0DAT=0x%08x", GP0DAT);
      LogDebug( "DO: #AABBBBBB(data): GP0SET=0x%08x", GP0SET);
      LogDebug( "DO: #AABBBBBB(data): GP0CLR=0x%08x", GP0CLR);
      LogDebug( "DO: #AABBBBBB(data): GP1DAT=0x%08x", GP1DAT);
      LogDebug( "DO: #AABBBBBB(data): GP1SET=0x%08x", GP1SET);
      LogDebug( "DO: #AABBBBBB(data): GP1CLR=0x%08x", GP1CLR);
      LogDebug( "DO: #AABBBBBB(data): GP2DAT=0x%08x", GP2DAT);
      LogDebug( "DO: #AABBBBBB(data): GP2SET=0x%08x", GP2SET);
      LogDebug( "DO: #AABBBBBB(data): GP2CLR=0x%08x", GP2CLR);
      LogDebug( "DO: #AABBBBBB(data): GP3DAT=0x%08x", GP3DAT);
      LogDebug( "DO: #AABBBBBB(data): GP3SET=0x%08x", GP3SET);
      LogDebug( "DO: #AABBBBBB(data): GP3CLR=0x%08x", GP3CLR);
      LogDebug( "DO: #AABBBBBB(data): GP4DAT=0x%08x", GP4DAT);
      LogDebug( "DO: #AABBBBBB(data): GP4SET=0x%08x", GP4SET);
      LogDebug( "DO: #AABBBBBB(data): GP4CLR=0x%08x", GP4CLR);
      */

      //RESPONSE.PREPARATION
      strResponse[ 0] = '!';
      strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc(2)];
      strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc(3)];

      if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);


      //RESPONSE.OUT
      LedOn( LED_TX);
      RS485_SetToTransmit();
      printf( "%s\r", strResponse);
      RS485_SetToReceive();

      return;
    }
    else
      
    if( ( gl_cCheckSummUsageDO  && nFirstCommandLen == 14) ||
        ( !gl_cCheckSummUsageDO && nFirstCommandLen == 12)) {

      LogDebug( "DO: Recognized Digital Startup Data Out set command (#{AA}{S}{BBBBBB}(data))");
      //COMMAND.ACCEPTED
      LedOn( LED_RX);

      //COMMAND.ACTION
      strncpy( ( char *) acMask, &gl_cpCircleBuffer[ cb_getInd_inc(5)], 6);
      unChannel = strtol( ( char *) acMask, NULL, 16);

      LogDebug( "DO: #{AA}{S}{BBBBBB}{data}: channel=%d (0x%06x)", unChannel, unChannel);
      LogDebug( "DO: #{AA}{S}{BBBBBB}{data}: gl_cpCircleBuffer[ cb_getInd_inc(11)]='%c'", gl_cpCircleBuffer[ cb_getInd_inc(11)]);

      if( gl_cpCircleBuffer[ cb_getInd_inc(11)] == '0') {
        //drop bit
        if( unChannel & 0x000001) gl_ushStartupValueDoL &= ~(0x0001);   // TU1 = P2.3
        if( unChannel & 0x000002) gl_ushStartupValueDoL &= ~(0x0002);   // TU2 = P4.6
        if( unChannel & 0x000004) gl_ushStartupValueDoL &= ~(0x0004);   // TU3 = P4.7
        if( unChannel & 0x000008) gl_ushStartupValueDoL &= ~(0x0008);   // TU4 = P0.6

        if( unChannel & 0x000010) gl_ushStartupValueDoL &= ~(0x0010);   // TU5 = P0.2
        if( unChannel & 0x000020) gl_ushStartupValueDoL &= ~(0x0020);   // TU6 = P3.0
        if( unChannel & 0x000040) gl_ushStartupValueDoL &= ~(0x0040);   // TU7 = P3.1
        if( unChannel & 0x000080) gl_ushStartupValueDoL &= ~(0x0080);   // TU8 = P3.2


        if( unChannel & 0x000100) gl_ushStartupValueDoL &= ~(0x0100);   // TU9 = P3.3
        if( unChannel & 0x000200) gl_ushStartupValueDoL &= ~(0x0200);   //TU10 = P2.4
        if( unChannel & 0x000400) gl_ushStartupValueDoL &= ~(0x0400);   //TU11 = P0.3
        if( unChannel & 0x000800) gl_ushStartupValueDoL &= ~(0x0800);   //TU12 = P2.5

        if( unChannel & 0x001000) gl_ushStartupValueDoL &= ~(0x1000);   //TU13 = P2.6
        if( unChannel & 0x002000) gl_ushStartupValueDoL &= ~(0x2000);   //TU14 = P3.4
        if( unChannel & 0x004000) gl_ushStartupValueDoL &= ~(0x4000);   //TU15 = P3.5
        if( unChannel & 0x008000) gl_ushStartupValueDoL &= ~(0x8000);   //TU16 = P0.4


        if( unChannel & 0x010000) gl_ushStartupValueDoH &= ~(0x0001);   //TU17 = P0.5
        if( unChannel & 0x020000) gl_ushStartupValueDoH &= ~(0x0002);   //TU18 = P2.0
        if( unChannel & 0x040000) gl_ushStartupValueDoH &= ~(0x0004);   //TU19 = P0.7
        if( unChannel & 0x080000) gl_ushStartupValueDoH &= ~(0x0008);   //TU20 = P3.6
      }
      else {
        //raise bit
        if( unChannel & 0x000001) gl_ushStartupValueDoL |= (0x0001);   // TU1 = P2.3
        if( unChannel & 0x000002) gl_ushStartupValueDoL |= (0x0002);   // TU2 = P4.6
        if( unChannel & 0x000004) gl_ushStartupValueDoL |= (0x0004);   // TU3 = P4.7
        if( unChannel & 0x000008) gl_ushStartupValueDoL |= (0x0008);   // TU4 = P0.6

        if( unChannel & 0x000010) gl_ushStartupValueDoL |= (0x0010);   // TU5 = P0.2
        if( unChannel & 0x000020) gl_ushStartupValueDoL |= (0x0020);   // TU6 = P3.0
        if( unChannel & 0x000040) gl_ushStartupValueDoL |= (0x0040);   // TU7 = P3.1
        if( unChannel & 0x000080) gl_ushStartupValueDoL |= (0x0080);   // TU8 = P3.2


        if( unChannel & 0x000100) gl_ushStartupValueDoL |= (0x0100);   // TU9 = P3.3
        if( unChannel & 0x000200) gl_ushStartupValueDoL |= (0x0200);   //TU10 = P2.4
        if( unChannel & 0x000400) gl_ushStartupValueDoL |= (0x0400);   //TU11 = P0.3
        if( unChannel & 0x000800) gl_ushStartupValueDoL |= (0x0800);   //TU12 = P2.5

        if( unChannel & 0x001000) gl_ushStartupValueDoL |= (0x1000);   //TU13 = P2.6
        if( unChannel & 0x002000) gl_ushStartupValueDoL |= (0x2000);   //TU14 = P3.4
        if( unChannel & 0x004000) gl_ushStartupValueDoL |= (0x4000);   //TU15 = P3.5
        if( unChannel & 0x008000) gl_ushStartupValueDoL |= (0x8000);   //TU16 = P0.4


        if( unChannel & 0x010000) gl_ushStartupValueDoH |= (0x0001);   //TU17 = P0.5
        if( unChannel & 0x020000) gl_ushStartupValueDoH |= (0x0002);   //TU18 = P2.0
        if( unChannel & 0x040000) gl_ushStartupValueDoH |= (0x0004);   //TU19 = P0.7
        if( unChannel & 0x080000) gl_ushStartupValueDoH |= (0x0008);   //TU20 = P3.6
      }

      //save to flash
      save_params( SAVE_MASK_DO);

      //RESPONSE.PREPARATION
      strResponse[ 0] = '!';
      strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc(2)];
      strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc(3)];

      if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);

      
      //RESPONSE.OUT
      LedOn( LED_TX);
      RS485_SetToTransmit();
      printf( "%s\r", strResponse);
      RS485_SetToReceive();

      return;
    }
    else {
      LogInfo( "DO: Unknown #-command");
      //COMMAND.NOT.ACCEPTED
      LedOn( LED_ERR);

      //RESPONSE.PREPARATION
      sprintf( strResponse, "?%02X", gl_cAddrDigitalOutput);
      if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);

      //RESPONSE.OUT
      RS485_SetToTransmit();
      printf( "%s\r", strResponse);
      RS485_SetToReceive();

      return;
    }
  }
  
  if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '$' ) {
    // **************************************************************************************
    //Команды запросов $AA

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '6' ) {
      // **************************************************************************************
      //  ${AA}6
      
      if( ( gl_cCheckSummUsageDO  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageDO && nFirstCommandLen == 5)) {
            
        LogDebug( "DO: Recognized Digital Current Data Out request command ${AA}6 (what is currently set)");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //COMMAND.ACTION
        if( GP2DAT & 0x080000) un_GPOStates += 0x00001;   // TU1 = P2.3
        if( GP4DAT & 0x400000) un_GPOStates += 0x00002;   // TU2 = P4.6
        if( GP4DAT & 0x800000) un_GPOStates += 0x00004;   // TU3 = P4.7
        if( GP0DAT & 0x400000) un_GPOStates += 0x00008;   // TU4 = P0.6

        if( GP0DAT & 0x040000) un_GPOStates += 0x00010;   // TU5 = P0.2
        if( GP3DAT & 0x010000) un_GPOStates += 0x00020;   // TU6 = P3.0
        if( GP3DAT & 0x020000) un_GPOStates += 0x00040;   // TU7 = P3.1
        if( GP3DAT & 0x040000) un_GPOStates += 0x00080;   // TU8 = P3.2


        if( GP3DAT & 0x080000) un_GPOStates += 0x00100;   // TU9 = P3.3
        if( GP2DAT & 0x100000) un_GPOStates += 0x00200;   //TU10 = P2.4
        if( GP0DAT & 0x080000) un_GPOStates += 0x00400;   //TU11 = P0.3
        if( GP2DAT & 0x200000) un_GPOStates += 0x00800;   //TU12 = P2.5

        if( GP2DAT & 0x400000) un_GPOStates += 0x01000;   //TU13 = P2.6
        if( GP3DAT & 0x100000) un_GPOStates += 0x02000;   //TU14 = P3.4
        if( GP3DAT & 0x200000) un_GPOStates += 0x04000;   //TU15 = P3.5
        if( GP0DAT & 0x100000) un_GPOStates += 0x08000;   //TU16 = P0.4

        if( GP0DAT & 0x200000) un_GPOStates += 0x10000;   //TU17 = P0.5
        if( GP2DAT & 0x010000) un_GPOStates += 0x20000;   //TU18 = P2.0
        if( GP0DAT & 0x800000) un_GPOStates += 0x40000;   //TU19 = P0.7
        if( GP3DAT & 0x400000) un_GPOStates += 0x80000;   //TU20 = P3.6

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = '0';
        sprintf( &strResponse[2], "%05X", un_GPOStates);
        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "DO: Wrong formatted ${AA}6 command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrDigitalOutput);
        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      
    }
    else
      
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'M' ) {
      // **************************************************************************************
      // ${AA}M
      
      if( ( gl_cCheckSummUsageDO  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageDO && nFirstCommandLen == 5)) {
            
        LogDebug( "DO: Get module name command ${AA}M");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //COMMAND.ACTION

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];
        strResponse[ 3] = 'G';
        strResponse[ 4] = 'P';
        strResponse[ 5] = 'O';
        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);
            
        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "DO: Wrong formatted ${AA}M command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrDigitalOutput);
        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);
        
        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();
        return;
      }
    }
    else
      
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'S' ) {
      // **************************************************************************************
      // ${AA}S6
      
      if( ( gl_cCheckSummUsageDO  && nFirstCommandLen == 8) ||
          ( !gl_cCheckSummUsageDO && nFirstCommandLen == 6)) {
            
        LogDebug( "DO: Recognized Digital Startup Data Out request command ${AA}S6 (what will be at the start)");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "!%02X%04X", gl_ushStartupValueDoH, gl_ushStartupValueDoL);
        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "DO: Wrong formatted ${AA}S6 command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrDigitalOutput);
        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);
        
        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();
        return;
      }
    }
    else

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '2' ) {
      // **************************************************************************************
      // ${AA}2
      
      if( ( gl_cCheckSummUsageDO  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageDO && nFirstCommandLen == 5)) {
            
        LogDebug( "AI: Recognized Config request command ${AA}2");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';

        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];    //AA
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];    //AA

        strResponse[ 3] = '0';                  //TT
        strResponse[ 4] = '0';                  //TT

        strResponse[ 5] = '0';                  //CC
        strResponse[ 6] = '0';                  //CC

        strResponse[ 7] = gl_cCheckSummUsageDO ? '4':'0';   //FF
        strResponse[ 8] = '0';                              //FF

        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();
        
        return;
      }
      else {
        LogInfo( "AO: Wrong formatted ${AA}2 command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrDigitalOutput);
        if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);
        
        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }

    LogDebug( "DO: Unknown $command.");
    //COMMAND.NOT.ACCEPTED
    LedOn( LED_ERR);

    //RESPONSE.PREPARATION
    sprintf( strResponse, "?%02X", gl_cAddrDigitalOutput);
    if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);

    //RESPONSE.OUT
    RS485_SetToTransmit();
    printf( "%s\r", strResponse);
    RS485_SetToReceive();

    return;
  }

  LogDebug( "DO: Unknown command (neither $ nor # at the beginning).");
  //COMMAND.NOT.ACCEPTED
  LedOn( LED_ERR);

  //RESPONSE.PREPARATION
  sprintf( strResponse, "?%02X", gl_cAddrDigitalOutput);
  if( gl_cCheckSummUsageDO) AddCheckSumm( strResponse);
  
  //RESPONSE.OUT
  RS485_SetToTransmit();
  printf( "%s\r", strResponse);
  RS485_SetToReceive();
}
