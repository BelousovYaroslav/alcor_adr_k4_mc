#ifndef LOGGER_H
  #define LOGGER_H

  #define LOG_LEVEL_OFF   0
  #define LOG_LEVEL_FATAL 1
  #define LOG_LEVEL_ERROR 2
  #define LOG_LEVEL_WARN  3
  #define LOG_LEVEL_INFO  4
  #define LOG_LEVEL_DEBUG 5
  #define LOG_LEVEL_TRACE 6

  void LogTrace( const char *format, ...);
  void LogDebug( const char *format, ...);
  void LogInfo(  const char *format, ...);
  void LogWarn(  const char *format, ...);
  void LogError( const char *format, ...);
  void LogFatal( const char *format, ...);

  void RS485_SetToTransmit( void);
  void RS485_SetToReceive( void);

#endif
