#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cb.h"
#include "logger.h"

extern char *gl_cpCircleBuffer;                 //кольцевой буфер байт входящих команд

int ValidateCheckSumm( int nLen) {
  int i;
  char cCS = 0;
  char acCSgiven[3] = { 0, 0, 0};
  char cCSgiven;
  
  LogDebug( "VALIDATE_CS: nLen = %d", nLen);
  LogDebug( "VALIDATE_CS: %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X", 
                        gl_cpCircleBuffer[ cb_getInd_inc(0)],
                        gl_cpCircleBuffer[ cb_getInd_inc(1)],
                        gl_cpCircleBuffer[ cb_getInd_inc(2)],
                        gl_cpCircleBuffer[ cb_getInd_inc(3)],
                        gl_cpCircleBuffer[ cb_getInd_inc(4)],
                        gl_cpCircleBuffer[ cb_getInd_inc(5)],
                        gl_cpCircleBuffer[ cb_getInd_inc(6)],
                        gl_cpCircleBuffer[ cb_getInd_inc(7)],
                        gl_cpCircleBuffer[ cb_getInd_inc(8)],
                        gl_cpCircleBuffer[ cb_getInd_inc(9)]);

  acCSgiven[ 0] = gl_cpCircleBuffer[ cb_getInd_inc( nLen - 2)];
  acCSgiven[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( nLen - 1)];

  cCSgiven = strtol( acCSgiven, NULL, 16);

  //for( i=0; i<nLen-3; cCS += gl_cpCircleBuffer[ cb_getInd_inc( i++)]);
  for( i=1; i<nLen-2; i++) {
    cCS += gl_cpCircleBuffer[ cb_getInd_inc( i)];
    LogDebug( "VALIDATE_CS: adding byte N%d = %02X... running summ=%02X", i, gl_cpCircleBuffer[ cb_getInd_inc( i)], cCS);
  }

  LogDebug( "VALIDATE_CS: Given=0x%02X  Calculated=0x%02X", cCSgiven, cCS);
  return( cCS == cCSgiven);
}

void AddCheckSumm( char *cmd) {
  int nLen = strlen( cmd), i;
  unsigned char cCS = 0;
  
  //for( i = 0; i < nLen; cCS += cmd[i++]);
  for( i = 0; i < nLen; i++) {
    cCS += cmd[i];
    LogDebug( "ADD_CS: adding byte N%d = %02X... running summ=%02X", i, cmd[i], cCS);
  }
  
  LogDebug( "ADD_CS: Adding 0x%02X", cCS);
  sprintf( &cmd[nLen], "%02X", cCS);
}
