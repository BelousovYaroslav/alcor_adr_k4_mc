#include <ADuC7026.h>
//#include "serial.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "flashEE.h"
#include "settings.h"
#include "errors.h"
#include "version.h"
#include "debug.h"
#include "logger.h"
#include "cb.h"

int gl_nLogLevel = LOG_LEVEL_OFF;           //уровень логирования: 0-off 1-min ... 6-max (see logger.h)
int gl_nT1ValRxLedDown = 0, gl_nT1ValTxLedDown = 0, gl_nT1ValErrLedDown = 0;

extern void processIncomingCommand( void);

//********************
//ПАРАМЕТРЫ ХРАНИМЫЕ ВО ФЛЭШ ПАМЯТИ
//********************
unsigned short gl_ushBaseSettingsPage;        //стартовая страница для хранения параметров (TODO: DI=BASE, DO=BASE+1page, AI=BASE+2page, AO=BASE+3page)

unsigned char  gl_cAddrDigitalInput   = 1;    //адрес блока цифровых входов     (Циф вход)  TS  DI
unsigned char  gl_cCheckSummUsageDI = 0;      //использование CheckSumm

unsigned char  gl_cAddrDigitalOutput  = 2;    //адрес блока цифровых выходов    (Циф выход) TU  DO
unsigned char  gl_cCheckSummUsageDO = 0;      //использование CheckSumm
unsigned short gl_ushStartupValueDoL;         //Стартовые состояния каналов выходов (младшие 16 каналов)
unsigned short gl_ushStartupValueDoH;         //Стартовые состояния каналов выходов (старшие 16 каналов)

unsigned char  gl_cAddrAnalogueInput  = 3;    //адрес блока аналоговых входов   (АЦП)       TI  AI
unsigned char  gl_cCheckSummUsageAI = 0;      //использование CheckSumm
double         gl_dblAI_calA = 1.;            //A-коэффициент калибровки

unsigned char  gl_cAddrAnalogueOutput = 4;    //адрес блока аналоговых выходов  (ЦАП)           AO
unsigned char  gl_cCheckSummUsageAO = 0;      //использование CheckSumm
unsigned short gl_aushCurrentDacValues[5];    //текущие состояния каналов
unsigned short *gl_paushCurrentDacValues;     //указатель на массив текущих состояний каналов
unsigned short gl_aushStartUpDacValues[5];    //стартовые состояния каналов
unsigned short *gl_paushStartUpDacValues;     //указатель на массив стартовых состояний каналов
double         gl_dblAO_calA = 1.;            //A-коэффициент калибровки
double         gl_dblAO_calB = 0.;            //B-коэффициент калибровки

//********************
//ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ ВО ВСЕХ МОДУЛЯХ
//********************
char gl_c_EmergencyCode = 0;    //код ошибки прибора

// IN_COMMAND_BUF_LEN defined in   "cb.h"
char gl_caCircleBuffer[ IN_COMMAND_BUF_LEN];    //кольцевой буфер байт входящих команд
char *gl_cpCircleBuffer;                        //кольцевой буфер байт входящих команд
int  gl_nCircleBufferPut = 0;                   //индекс где мы можем положить следующий входящий байт
int  gl_nCircleBufferGet = -1;                  //индекс указывающий последний взятый символ
char gl_bCircleBufferSettings = 0;              //флаги кольцевого буфера:


char gl_bIncomingCommand = 0;                   //Флаг принятой входящей команды

signed short gl_ssh_adc[12] = { 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0,
                                0, 0 };       //текущее значения ADC
signed short *gl_sshp_adc;

#define BIT_0 1
#define BIT_1 2
#define BIT_2 4
#define BIT_3 8
#define BIT_4 16
#define BIT_5 32
#define BIT_6 64
#define BIT_7 128

//2014-08-27 - enabling external oscillator
static int new_clock_locked = 0;


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//обработчик прерываний
__irq void FIQ_Handler (void) {
  if( ( FIQSTA & UART_BIT) != 0) {
    char cIncoming = COMRX;

    if( gl_bCircleBufferSettings & BUFFER_OVERROUNDED) {
      //буфер закольцевался

      if( gl_bCircleBufferSettings & PUT_OVERROUNDED) {
        //PUT индекс точку 0 и он должен быть меньше чем индекс GET индекс

        if( gl_nCircleBufferGet > gl_nCircleBufferPut + 1)
          //у нас есть место куда положить байт - кладём его и увеличиваем индекс PUT
          gl_caCircleBuffer[ gl_nCircleBufferPut++] = cIncoming;
          
        else {
          //Кольцевой буфер переполнился! Есть входящий байт, а складывать нам его некуда

          //сбрасываем кольцевой буфер
          gl_nCircleBufferPut = 0;
          gl_nCircleBufferGet = -1;
          gl_bCircleBufferSettings = 0;
          
          //и начинаем как будто с нуля
          gl_caCircleBuffer[ gl_nCircleBufferPut++] = cIncoming;
        }

        //закольцовка буфера
        if( gl_nCircleBufferPut == IN_COMMAND_BUF_LEN) {
          gl_nCircleBufferPut = 0;
          gl_bCircleBufferSettings |= PUT_OVERROUNDED;
          gl_bCircleBufferSettings |= BUFFER_OVERROUNDED;
        }

      }
    }
    else {
      //буфер ещё не закольцовывался

      //просто кладём байтик в буфер и проверяем закольцованность
      gl_caCircleBuffer[ gl_nCircleBufferPut++] = cIncoming;

      //закольцовка буфера
      if( gl_nCircleBufferPut == IN_COMMAND_BUF_LEN) {
        gl_nCircleBufferPut = 0;
        gl_bCircleBufferSettings |= PUT_OVERROUNDED;
        gl_bCircleBufferSettings |= BUFFER_OVERROUNDED;
      }
    }

    if( cIncoming == 0x0D)
      gl_bIncomingCommand = 1;
  }
}


__irq void IRQ_Handler (void) 
{
  if (IRQSIG & WAKEUP_TIMER_BIT) {
    /* disable timer, clear interrupt flag */
    T2CON   = 0x00;
    T2CLRI  = 0x01;
    new_clock_locked = 1;
  }
}


double round( double val) {
  double lstd = val - floor( val);
  if( lstd < .5) return floor( val);
  else return ceil( val);
}


int cb_getInd_inc( int inc) {
  return (( gl_nCircleBufferGet + inc) % IN_COMMAND_BUF_LEN);
}

int putchar_nocheck( int ch)  {                   /* Write character to Serial Port w/o adding \r to \n */
  while( !( 0x020 == ( COMSTA0 & 0x020))) {}
  return( COMTX = ch);
}

void InitBaudRate115200() {
  //DL = 41779200 / 16 / 2 / 115200 = 11.(3) = 11
  //RESULT=M+N/2048 = 41779200 / 115200 / (2^(CD=0)=1) / 16 / (DL=11) / 2 = 1.(03)
  //M=FLOOR(1.(03))=1
  //N=(1.(03)-1)*2048=62.(06)=62=0x03E
  //      8             8
  //  1 0 0   0     1   0 0 0
  //  0 0 1   1     1   1 1 0
  //     3             E
  //COMDIV2 = 0x883E
  
  
  
  //WORK PARAMETERS FOR PRECISE 115200
  COMDIV0 = 0x0B;   //=DL(low byte)
  COMDIV1 = 0x00;   //=DL(high byte)
  COMDIV2 = 0x883E;;
}

void InitBaudRate256000() {
  //DL = 41.78 * 10^6 / 2 ^ (CD=0) * 1/16 * 1/ (2*Baudrate)
  //RESULT = (M+N/2048) = 41.78 * 10^6 / ( 2 ^ (CD=0) * 16 *  2 * Baudrate)
  // M = Floor ( RESULT)
  // N = (RESULT - M) * 2048

  
  //WORK PARAMETERS FOR PRECISE 256000
  COMDIV0 = 0x05;
  COMDIV1 = 0x00;
  COMDIV2 = 0x8829;
}

void InitBaudRate512000() {
  //DL = 41.78e6 / 16 / 2 / 512000 = 2.55 = 2
  //M+N/2048 = 41779200 / 512000 / (2^(CD=0)=1) / 16 / (DL=2) / 2 = 1.275
  //M=FLOOR(1.275)=1
  //N=(1.275-1)*2048=563.2=563=0x233
  //      8             A
  //  1 0 0   0     1   0 1 0
  //  0 0 1   1     0   0 1 1
  //     3             3
  //COMDIV2 = 0x8A33
  
  COMDIV0 = 2;
  COMDIV1 = 0x00;
  COMDIV2 = 0x8A33;
}

void InitBaudRate921600() {
  /*************** */
  /*   921 600     */
  /*************** */
  //DL = 41.78e6 / 921600 / (2^(CD=0)=1) / 16 / 2 = 1,416693793
  //DL=1
  //M+N/2048 = 41.78e6 / 921600 / (2^(CD=0)=1) / 16/ (DL=1) / 2 = 1,416693793
  //M=1
  //N=853=0x355
  //      8             B
  //  1 [0 0] [0     1]  0 1 1
  //  0  1 0   1     0   1 0 1
  //     5             5
  //COMDIV2 = 0x8B55
  
  COMDIV0 = 1;      //младший байт делителя (DL)
  COMDIV1 = 0x00;   //старший байт делителя (DL)
  COMDIV2 = 0x8B55; //16-разрядный регистр дробного делителя
                    //15 - FBEN - бит разрешения работы генератора с дробным делителем
                    //14-13 reserved
                    //12-11 M
                    //10-0  N
  
  /****************************************************************************************** */
}

void pause( int n) {
  unsigned int prval, chk;

  //ASSUMING that T1 act with 32kHz rate
  prval = T1VAL;
  chk = (( T1LD + prval - T1VAL) % T1LD);

  while( chk < n)
    chk = (( T1LD + prval - T1VAL) % T1LD);

}

void SimpleTest() {
  float f = 1.23;
  int n = ( int) floor( f);
  char buf[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  f -= n;
  sprintf( buf, "%.03f", f);

  f = 0.123;
  printf( "%.03f\n", f);
  f = 1.123;
  printf( "%.03f\n", f);
  f = 2.123;
  printf( "%.03f\n", f);

  //RS485_SetToTransmit();
  //printf("%02d%s",n, &buf[1]);
  //RS485_SetToReceive();
}

int main() {
  int time = 20000;
  int i = 0, j; // k;
  double dblValue;
  unsigned short ushDacCode;
  
  //int n;
  //int t2val_old;



  
  gl_cpCircleBuffer = gl_caCircleBuffer;

  //SETTING POINTERS
  gl_sshp_adc = gl_ssh_adc;
  gl_paushStartUpDacValues = gl_aushStartUpDacValues;
  gl_paushCurrentDacValues = gl_aushCurrentDacValues;

  // Setup tx & rx pins on P1.0 and P1.1
  GP0CON = 0x00;
  GP1CON = 0x011;       //*** 0x011 = 0001 0001 = 00 01 00 01
                        //*** 01 - Выбор функции вывода порта P1.0
                        //*** 00 - Reserved
                        //*** 01 - Выбор функции вывода порта P1.1
                        //*** 00 - Reserved
                        //*** Функция:
                        //***         00    01    10    11
                        //***   P1.0  GPIO  SIN   SCL0  PLAI[0]
                        //***   P1.1  GPIO  SOUT  SDA0  PLAI[1]

  // Start setting up UART
  COMCON0 = 0x080;      // Setting DLAB

  // Setting DIV0 and DIV1 to DL calculated
  InitBaudRate115200();
  //InitBaudRate256000();
  //InitBaudRate512000();
  //InitBaudRate921600();


  COMCON0 = 0x007;      // Clearing DLAB

  //SimpleTest();
  //while(1);


  LogDebug( "MAIN: flashEE configuration");
  //**********************************************************************
  // Конфигурация флэш-памяти FlashEE
  //**********************************************************************
  flashEE_configure();


  LogDebug( "MAIN: flashEE load params");
  //**********************************************************************
  // Загрузка параметров из флэш-памяти
  //**********************************************************************
  load_params();


  //**********************************************************************
  // Включение и выставка начальных значений ЦАПов
  //**********************************************************************

  // ЦАП 0   <<<<<<<<<<<<<<<<<<<<<<<<<
  LogDebug( "MAIN: DAC0 configure & set out values");
  DAC0CON = 0x11;           // Конфигурируем ЦАП: (0x12 = 0001 0010 = 00 0 1 0 0 10)
                            // XX X X X X 10      0-Vref range
                            // XX X X X 0 XX      Reserved. Should be left at 0
                            // XX X X 0 X XX      Reserved. Should be left at 0
                            // XX X 1 X X XX      DAC clear bit. Set to enable normal DAC operation.
                            // XX 0 X X X XX      DAC update rate. Cleared to update the DAC using HCLK (core clock)
                            // 00 X X X X XX      Reserved.
  //DAC0DAT &= ~(0x0FFF0000); //output 0.

  dblValue = gl_paushStartUpDacValues[0];// * gl_dblAO_calA + gl_dblAO_calB;
  LogDebug( "MAIN: DAC0 value from startup = %.02f", dblValue);
  if( dblValue < 0) {
    ushDacCode = ( unsigned short) ( -gl_dblAO_calB / gl_dblAO_calA * 4096 / 3.0);
    LogWarn( "MAIN: DAC0 startup value is negative. Applying correction to zero");
    LogWarn( "MAIN: DAC0 startup value is negative. %04X -> %04X", gl_paushStartUpDacValues[0], ushDacCode);
  }
  else
    ushDacCode = gl_paushStartUpDacValues[0];
  LogDebug( "MAIN: DAC0 applying startup code: %04X", ushDacCode);
  DAC0DAT = ( int) ushDacCode << 16;
  gl_paushCurrentDacValues[0] = ushDacCode;


  // ЦАП 1   <<<<<<<<<<<<<<<<<<<<<<<<<
  LogDebug( "MAIN: DAC1 configure & set out values");
  DAC1CON = 0x11;           // Конфигурируем ЦАП: (0x12 = 0001 0010 = 00 0 1 0 0 10)
                            // XX X X X X 10      0-Vref range
                            // XX X X X 0 XX      Reserved. Should be left at 0
                            // XX X X 0 X XX      Reserved. Should be left at 0
                            // XX X 1 X X XX      DAC clear bit. Set to enable normal DAC operation.
                            // XX 0 X X X XX      DAC update rate. Cleared to update the DAC using HCLK (core clock)
                            // 00 X X X X XX      Reserved.
  //DAC1DAT &= ~(0x0FFF0000); //output 0.

  dblValue = gl_paushStartUpDacValues[1];// * gl_dblAO_calA + gl_dblAO_calB;
  if( dblValue < 0) {
    ushDacCode = ( unsigned short) ( -gl_dblAO_calB / gl_dblAO_calA * 4096 / 3.0);
  }
  else
    ushDacCode = gl_paushStartUpDacValues[1];
  DAC1DAT = ( int) ushDacCode << 16;
  gl_paushCurrentDacValues[1] = ushDacCode;


  // ЦАП 2   <<<<<<<<<<<<<<<<<<<<<<<<<
  LogDebug( "MAIN: DAC2 configure & set out values");
  DAC2CON = 0x11;           // Конфигурируем ЦАП: (0x12 = 0001 0010 = 00 0 1 0 0 10)
                            // XX X X X X 10      0-Vref range
                            // XX X X X 0 XX      Reserved. Should be left at 0
                            // XX X X 0 X XX      Reserved. Should be left at 0
                            // XX X 1 X X XX      DAC clear bit. Set to enable normal DAC operation.
                            // XX 0 X X X XX      DAC update rate. Cleared to update the DAC using HCLK (core clock)
                            // 00 X X X X XX      Reserved.
  //DAC2DAT &= ~(0x0FFF0000); //output 0.

  dblValue = gl_paushStartUpDacValues[2];// * gl_dblAO_calA + gl_dblAO_calB;
  if( dblValue < 0) {
    ushDacCode = ( unsigned short) ( -gl_dblAO_calB / gl_dblAO_calA * 4096 / 3.0);
  }
  else
    ushDacCode = gl_paushStartUpDacValues[2];
  DAC2DAT = ( int) ushDacCode << 16;
  gl_paushCurrentDacValues[2] = ushDacCode;


  // ЦАП 3   <<<<<<<<<<<<<<<<<<<<<<<<<
  LogDebug( "MAIN: DAC3 configure & set out values");
  DAC3CON = 0x11;           // Конфигурируем ЦАП: (0x12 = 0001 0010 = 00 0 1 0 0 10)
                            // XX X X X X 10      0-Vref range
                            // XX X X X 0 XX      Reserved. Should be left at 0
                            // XX X X 0 X XX      Reserved. Should be left at 0
                            // XX X 1 X X XX      DAC clear bit. Set to enable normal DAC operation.
                            // XX 0 X X X XX      DAC update rate. Cleared to update the DAC using HCLK (core clock)
                            // 00 X X X X XX      Reserved.
  //DAC3DAT &= ~(0x0FFF0000); //output 0.

  dblValue = gl_paushStartUpDacValues[3];// * gl_dblAO_calA + gl_dblAO_calB;
  if( dblValue < 0) {
    ushDacCode = ( unsigned short) ( -gl_dblAO_calB / gl_dblAO_calA * 4096 / 3.0);
  }
  else
    ushDacCode = gl_paushStartUpDacValues[3];
  DAC3DAT = ( int) ushDacCode << 16;
  gl_paushCurrentDacValues[3] = ushDacCode;



  //**********************************************************************
  // Выставка стартап значений ЦАП
  //**********************************************************************
  // Example how to set [x Volt]
  // DAC0DAT = (( int) ( ( x / 3.0) * 4095.0)) << 16;

  /*
  gl_paushStartUpDacValues[0] = 0;
  gl_paushStartUpDacValues[1] = 0;
  gl_paushStartUpDacValues[2] = 0;
  gl_paushStartUpDacValues[3] = 0;
  gl_paushStartUpDacValues[4] = 0;
  */

  LogDebug( "MAIN: DAC0 out values configuration... (already set) %04X", gl_paushStartUpDacValues[0]);
  LogDebug( "MAIN: DAC1 out values configuration... (already set) %04X", gl_paushStartUpDacValues[1]);
  LogDebug( "MAIN: DAC2 out values configuration... (already set) %04X", gl_paushStartUpDacValues[2]);
  LogDebug( "MAIN: DAC3 out values configuration... (already set) %04X", gl_paushStartUpDacValues[3]);


  LogDebug( "MAIN: GPIO in/out configuration...");
  //**********************************************************************
  // GPIO-НАПРАВЛЕНИЯ

  //P0
  //p0.7  p0.6  p0.5  p0.4    p0.3  p0.2  p0.1  p0.0
  // o     o     o     o       o     o     o     -  

  //выключение (в 0)
  //nothing

  //включение (в 1)
  // 1     1     1     1       1     1     1     -  
  //          F                        E
  GP0DAT |= ( unsigned long) ( 0xFE) << 24;

  GP4DAT &= ~(1 << (16 + 3));  //DAC4-5 switch to DAC4     (p0.1) = 0


  //P1
  //p1.7  p1.6  p1.5  p1.4    p1.3  p1.2  p1.1  p1.0
  // i     i     i     i       i     i     -     -  
  
  //выключение (в 0)
  // 1     1     1     1       1     1     0     0
  //          F                         C
  GP1DAT &= ~( ( unsigned long) ( 0xFC) << 24);

  //включение (в 1)
  //nothing



  //P2
  //p2.7  p2.6  p2.5  p2.4    p2.3  p2.2  p2.1  p2.0
  // i     o     o     o       o     i     i     o  

  //выключение (в 0)
  // 1     0     0     0       0     1     1     0  
  //          8                         6
  GP2DAT &= ~( ( unsigned long) ( 0x86) << 24);
  //включение (в 1)
  // 0     1     1     1       1     0     0     1  
  //          7                         9
  GP2DAT |= (0x79) << 24;



  //P3
  //p3.7  p3.6  p3.5  p3.4    p3.3  p3.2  p3.1  p3.0
  // i     o     o     o       o     o     o     o  

  //выключение (в 0)
  // 1     0     0     0       0     0     0     0  
  //          8                         0
  GP3DAT &= ~( ( unsigned long) ( 0x80) << 24);
  //включение (в 1)
  // 0     1     1     1       1     1     1     1  
  //          7                         F
  GP3DAT |= ( unsigned long) ( 0x7F) << 24;



  //P4
  //p4.7  p4.6  p4.5  p4.4    p4.3  p4.2  p4.1  p4.0
  // o     o     o     o       o     i     i     i  

  //выключение (в 0)
  // 0     0     0     0       0     1     1     1  
  //          0                         7
  GP4DAT &= ( unsigned long) ( 0x07) << 24;
  //включение (в 1)
  // 1     1     1     1       1     0     0     0  
  //          F                         8
  GP4DAT |= ( unsigned long) ( 0xF8) << 24;


  GP4DAT |= 1 << (16 + 3);  //ErrLED off     (p4.3) = 1
  GP4DAT |= 1 << (16 + 4);  //TxLED off      (p4.4) = 1
  GP4DAT |= 1 << (16 + 5);  //RxLED off      (p4.5) = 1

  /*
  do {
    for( i=0; i<100; i++) {
      GP2DAT |= 1 << (16 + 3);      //TempLED on     	 (p2.3) = 1
      for( j=0; j<1000000; j++);
      GP2DAT &= ~( 1 << (16 + 3));  //TempLED off      (p2.3) = 0
    }

    RS485_SetToTransmit();
    for( i=0; i<100; i++) {
      printf( "AA\n");
    }
    RS485_SetToReceive();

  } while(1);
  */


  //**********************************************************************
  // Включение прерывания по UART0 и ADC
  //**********************************************************************	
  FIQEN |= UART_BIT;
  COMIEN0 |= 1;

  LogDebug( "MAIN: Internal ADC configuration...");
  //**********************************************************************
  // Конфигурация АЦП
  //**********************************************************************	
  //FIQEN |= ADC_BIT;

  //0x20 = 0000 0000 0010 0000
  //0x20 = 000 000 00 0 0 1 00 000
  ADCCON &= 0x20;           // включаем АЦП

  while( time >=0)          // ждем указанное в datasheet время (5мксек) для полного включения АЦП
    time--;

  //0x624 = 0000 0110 0010 0100
  //0x624 = 000 001 10 0 0 1 00 100
  ADCCON = 0x623;           // Конфигурируем АЦП:
                            // 100 - непрерывное преобразование с управлением от программы
                            // 00 XXX - однополярный вход
                            // 1 XX XXX - (включенное питание АЦП)
                            // 0 X XX XXX - RESERVED vs (из другого datasheet) запрещенный ADCBusy
                            // 0 X X XX XXX - без старта преобразования
                            // 10 X X X XX XXX - преобразование 8 тактов
                            // 001 XX X X X XX XXX - тактирование [fADC / 2]
                            // 000 XXX XX X X X XX XXX - RESERVED

  ADCCP &= ~(0x1F);         // ставим 0-ой канал АЦП (выключаем [0:4] битики)
  REFCON &= 0xFE;            // отключаем внутренний ИОН от пина Vref (гасим 0 битик)




  LogDebug( "MAIN: Timers configuration...");
  //**********************************************************************
  // Конфигурация Timer0
  //**********************************************************************
  T0CON = 0x80;

  //**********************************************************************
  // Конфигурация Timer1
  //**********************************************************************
  T1CON = 0x2C0;  //32kHz tact
  T1LD = 0x100000;

  //**********************************************************************
  // Конфигурация внешнего осциллятора
  //**********************************************************************
  /* configuring Timer2 for Wake-up */
  //t2val_old = T2VAL;
  T2LD = 0x050;   // 5; 5 clocks @32.768kHz = 152ms
  T2CON = 0x480;  // Enable Timer2;

  //while ((T2VAL == t2val_old) || (T2VAL > 3)); // ensures timer is started...

  /* enabling Timer2 interrupt */
  IRQEN = 0x10;   // enable TIMER2 Wakeup IRQ

  /* setting power into NAP mode */
  PLLKEY1 = 0xAA;
  PLLCON  = 0x01;  // External Osc. Select.
  PLLKEY2 = 0x55;

  POWKEY1 = 0x01;
  POWCON  = 0x20;  // NAP Mode, 326kHZ Clock Divider
  POWKEY2 = 0xF4;

  /* waiting for switch to new clock */
  while (!new_clock_locked);

  /* disabling Timer2 interrupt */
  IRQEN &= ~(0x10);   // disable TIMER2 Wakeup IRQ

  //**********************************************************************
  // Конфигурация Timer2
  //**********************************************************************
  T2CON = 0x0C0;
  T2LD = 0x100000;

  


  LogDebug( "MAIN: GPIO out values configuration... %02X %04X", gl_ushStartupValueDoH, gl_ushStartupValueDoL);
  //**********************************************************************
  // GPIO-OUT ЗНАЧЕНИЯ
  if( gl_ushStartupValueDoL & 0x0001) GP2SET = 0x080000; else GP2CLR = 0x080000;   // TU1 = P2.3
  if( gl_ushStartupValueDoL & 0x0002) GP4SET = 0x400000; else GP4CLR = 0x400000;   // TU2 = P4.6
  if( gl_ushStartupValueDoL & 0x0004) GP4SET = 0x800000; else GP4CLR = 0x800000;   // TU3 = P4.7
  if( gl_ushStartupValueDoL & 0x0008) GP0SET = 0x400000; else GP0CLR = 0x400000;   // TU4 = P0.6

  if( gl_ushStartupValueDoL & 0x0010) GP0SET = 0x040000; else GP0CLR = 0x040000;   // TU5 = P0.2
  if( gl_ushStartupValueDoL & 0x0020) GP3SET = 0x010000; else GP3CLR = 0x010000;   // TU6 = P3.0
  if( gl_ushStartupValueDoL & 0x0040) GP3SET = 0x020000; else GP3CLR = 0x020000;   // TU7 = P3.1
  if( gl_ushStartupValueDoL & 0x0080) GP3SET = 0x040000; else GP3CLR = 0x040000;   // TU8 = P3.2


  if( gl_ushStartupValueDoL & 0x0100) GP3SET = 0x080000; else GP3CLR = 0x080000;   // TU9 = P3.3
  if( gl_ushStartupValueDoL & 0x0200) GP2SET = 0x100000; else GP2CLR = 0x100000;   //TU10 = P2.4
  if( gl_ushStartupValueDoL & 0x0400) GP0SET = 0x080000; else GP0CLR = 0x080000;   //TU11 = P0.3
  if( gl_ushStartupValueDoL & 0x0800) GP2SET = 0x200000; else GP2CLR = 0x200000;   //TU12 = P2.5

  if( gl_ushStartupValueDoL & 0x1000) GP2SET = 0x400000; else GP2CLR = 0x400000;   //TU13 = P2.6
  if( gl_ushStartupValueDoL & 0x2000) GP3SET = 0x100000; else GP3CLR = 0x100000;   //TU14 = P3.4
  if( gl_ushStartupValueDoL & 0x4000) GP3SET = 0x200000; else GP3CLR = 0x200000;   //TU15 = P3.5
  if( gl_ushStartupValueDoL & 0x8000) GP0SET = 0x100000; else GP0CLR = 0x100000;   //TU16 = P0.4


  if( gl_ushStartupValueDoH & 0x0001) GP0SET = 0x200000; else GP0CLR = 0x200000;   //TU17 = P0.5
  if( gl_ushStartupValueDoH & 0x0002) GP2SET = 0x010000; else GP2CLR = 0x010000;   //TU18 = P2.0
  if( gl_ushStartupValueDoH & 0x0004) GP0SET = 0x800000; else GP0CLR = 0x800000;   //TU19 = P0.7
  if( gl_ushStartupValueDoH & 0x0008) GP3SET = 0x400000; else GP3CLR = 0x400000;   //TU20 = P3.6

  /*
  //TEST DAC CODE
  while( 1) {
    for( i=0; i<4095; i+=500) {
      DAC0DAT = ( i << 16);
      pause( 32768);
    }
  }
  */

  //**********************************************************************
  // Запуск преобразования АЦП
  //**********************************************************************
  /*ADCCP = 0x01;
  //pause(10);
  ADCCON |= 0x80;
  */

  
  LogDebug( "MAIN: READY");

  //**********************************************************************
  // Основной цикл работы программы
  //**********************************************************************
  while( 1) {

    //**********************************************************************
    //Опрос АЦП
    //**********************************************************************
    //LogTrace( "MAIN: ADC poll ..... START");


    for( i=0; i<12; i++) {

      ADCCP = i;        //выставляем измеряемый канал

      pause( 10);
      
      //RS485_SetToTransmit();
      //printf("    %d  0x%04X\n", ADCSTA, ADCCON);
      //RS485_SetToReceive();
      
      ADCCON |= 0x03;   //очередное включение однократного измерения


      //RS485_SetToTransmit();
      //printf(".   %d  0x%04X\n", ADCSTA, ADCCON);
      //RS485_SetToReceive();

      ADCCON |= 0x80;   //запуск измерения

      //RS485_SetToTransmit();
      //printf(".. %d  0x%04X\n", ADCSTA, ADCCON);
      //RS485_SetToReceive();
      
      j=0;
      while( ( ADCSTA & 0x01) == 0) {
        //if( !j) printf("... %d  0x%04X\n", ADCSTA, ADCCON);
        j= (++j) % 100;
      }

      //printf("....%d  0x%04X\n", ADCSTA, ADCCON);

      gl_ssh_adc[ i] = ( ADCDAT >> 16);

      //LogTrace( "TI%d = ADC%02d: 0x%04X   ADCCON=0x%04X\n", i + 1, i, gl_ssh_adc[ i], ADCCON);
    }

    //LogTrace( "MAIN: ADC poll ..... DONE");
    
    //LogTrace( "ADC[0-11]: 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X    DAC0DAT: 0x%04x",
    //  gl_ssh_adc[ 0], gl_ssh_adc[ 1], gl_ssh_adc[ 2], gl_ssh_adc[ 3], gl_ssh_adc[ 4], gl_ssh_adc[ 5], 
    //  gl_ssh_adc[ 6], gl_ssh_adc[ 7], gl_ssh_adc[ 8], gl_ssh_adc[ 9], gl_ssh_adc[10], gl_ssh_adc[11], DAC0DAT);

    LogTrace( "ADC[0-11]: 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X 0x%04X",
      gl_ssh_adc[ 0], gl_ssh_adc[ 1], gl_ssh_adc[ 2], gl_ssh_adc[ 3], gl_ssh_adc[ 4], gl_ssh_adc[ 5], 
      gl_ssh_adc[ 6], gl_ssh_adc[ 7], gl_ssh_adc[ 8], gl_ssh_adc[ 9], gl_ssh_adc[10], gl_ssh_adc[11]);

    //**********************************************************************
    // Обработка буфера входящих команд
    //**********************************************************************
    //i = (++i) % 10000;
    //if( !i) {
    //  LogDebug( "CB_IC: %d  CB_GET: %d  CB_PUT: %d", gl_bIncomingCommand, gl_nCircleBufferGet, gl_nCircleBufferPut);
    //  for( i=0; i<10; i++) {
    //    RS485_SetToTransmit();
    //    printf( "0x%02X ", gl_caCircleBuffer[ i]);
    //    RS485_SetToReceive();
    //  }
    //  RS485_SetToTransmit();
    //  printf("\n");
    //  RS485_SetToReceive();
    //}

    if( gl_bIncomingCommand == 1) {
      LogDebug( "MAIN: Got incoming command! Going to process it!");
      processIncomingCommand();
    }

    if( gl_nT1ValErrLedDown != 0 && T1VAL < gl_nT1ValErrLedDown) {
      GP4DAT |= 1 << (16 + 3);  //ErrLED off       (p4.3) = 1
      gl_nT1ValErrLedDown = 0;
    }
    
    if( gl_nT1ValTxLedDown != 0 && T1VAL < gl_nT1ValTxLedDown) {
      GP4DAT |= 1 << (16 + 4);  //TxLED off      (p4.4) = 1
      gl_nT1ValTxLedDown = 0;
    }

    if( gl_nT1ValRxLedDown != 0 && T1VAL < gl_nT1ValRxLedDown) {
      GP4DAT |= 1 << (16 + 5);  //RxLED off      (p4.5) = 1
      gl_nT1ValRxLedDown = 0;
    }
  }
}
