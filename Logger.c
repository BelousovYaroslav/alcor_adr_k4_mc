#include <ADuC7026.h>
#include <stdio.h>
#include <stdarg.h>
//#include "serial.h"
#include "logger.h"

extern int gl_nLogLevel;

void RS485_SetToTransmit( void) {
  int i;
  
  GP3DAT |= 1 << (16 + 6);      //RX_TX --> TX       (p3.6) = 1
  for( i=0; i<2000; i++);
}

void RS485_SetToReceive( void) {
  int i;
  
  for( i=0; i<2000; i++);
  GP3DAT &= ~( 1 << (16 + 6));  //RX_TX --> RX       (p3.6) = 0
  for( i=0; i<2000; i++);
}

void LogTrace( const char *format, ...) {

  if( gl_nLogLevel >= LOG_LEVEL_TRACE) {
    va_list args;	
    va_start( args, format);

    RS485_SetToTransmit();

    printf( "TRACE: ");
    vprintf( format, args);
    putchar( 0x0D);
    va_end( args);

    RS485_SetToReceive();
  }
}

void LogDebug( const char *format, ...) {
  if( gl_nLogLevel >= LOG_LEVEL_DEBUG) {
    va_list args;
    va_start( args, format);

    RS485_SetToTransmit();
    printf( "DEBUG: ");
    vprintf( format, args);
    putchar( 0x0D);
    va_end( args);
    RS485_SetToReceive();
  }
}

void LogInfo( const char *format, ...) {
  if( gl_nLogLevel >= LOG_LEVEL_INFO) {
    va_list args;
    va_start( args, format);

    RS485_SetToTransmit();
    printf( "INFO:  ");
    vprintf( format, args);
    putchar( 0x0D);
    va_end( args);
    RS485_SetToReceive();
  }
}

void LogWarn( const char *format, ...) {
  if( gl_nLogLevel >= LOG_LEVEL_WARN) {
    va_list args;
    va_start( args, format);

    RS485_SetToTransmit();
    printf( "WARN:  ");
    vprintf( format, args);
    putchar( 0x0D);
    va_end( args);
    RS485_SetToReceive();
  }
}

void LogError( const char *format, ...) {
  if( gl_nLogLevel >= LOG_LEVEL_ERROR) {
    va_list args;
    va_start( args, format);

    RS485_SetToTransmit();
    printf( "ERROR: ");
    vprintf( format, args);
    putchar( 0x0D);
    va_end (args);
    RS485_SetToReceive();
  }
}

void LogFatal( const char *format, ...) {
  if( gl_nLogLevel >= LOG_LEVEL_FATAL) {
    va_list args;
    va_start( args, format);

    RS485_SetToTransmit();
    printf( "FATAL: ");
    vprintf( format, args);
    putchar( 0x0D);
    va_end (args);
    RS485_SetToReceive();
  }
}
