#include <ADuC7026.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "settings.h"
#include "logger.h"
#include "cb.h"
#include "checksumm.h"
#include "leds.h"

extern unsigned char   gl_cAddrAnalogueOutput;    //адрес блока аналоговых выходов  (ЦАП)            AO
extern unsigned char   gl_cCheckSummUsageAO;      //использование CheckSumm
extern unsigned short *gl_paushStartUpDacValues;  //стартовые состояния каналов
extern unsigned short *gl_paushCurrentDacValues;  //указатель на массив текущих состояний каналов
extern double          gl_dblAO_calA;             //A-коэффициент калибровки
extern double          gl_dblAO_calB;             //B-коэффициент калибровки

extern char *gl_cpCircleBuffer;                 //кольцевой буфер байт входящих команд
extern int  gl_nCircleBufferPut;                //индекс где мы можем положить следующий входящий байт
extern int  gl_nCircleBufferGet;                //индекс указывающий последний взятый символ
extern char gl_bCircleBufferSettings;           //флаги кольцевого буфера
extern char gl_bIncomingCommand;                //Флаг принятой входящей команды

//обработка команд ЦАП
void ProcessAOCommand( int nFirstCommandLen) {
  char strResponse[256];
  
  int i;
  int nChannel;

  char cByte;
  char acValue[7] = { 0, 0, 0, 0, 0, 0, 0};
  float fValue;

  unsigned short shValueCode;

  memset( strResponse, 0, 256);

  LogDebug( "AO: in with nFirstCommandLen=%d", nFirstCommandLen);


  //INCOMING CHECKSUM VERIFICATION
  if( gl_cCheckSummUsageAO && !ValidateCheckSumm( nFirstCommandLen)) {
    LedOn( LED_ERR);
    LogWarn( "AO: Incoming CheckSumm verification fail!");
    return;
  }
  
  if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '#' ) {
    //команды задания значений

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'A') {
      // **************************************************************************************
      // #{AA}A{data}
      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 12) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 10)) {

        LogDebug( "AO: Recognized #{AA}A{data} command");

        //PARAM.ANALYZE
        for( i=0; i<5; acValue[ i++] = gl_cpCircleBuffer[ cb_getInd_inc(5 + i)]);

        fValue = atof( acValue);
        if( fValue >= 0. && fValue <= 5.) {
          //COMMAND.ACCEPTED
          LedOn( LED_RX);
          LogDebug( "AO: #{AA}A{data}: value '%.3f'", fValue);

          //COMMAND.ACTION
          gl_dblAO_calA = fValue;
          save_params( SAVE_MASK_AO);

          //RESPONSE.PREPARATION
          strResponse[ 0] = '!';
          for( i=2; i<10; i++)
            strResponse[ i - 1] = gl_cpCircleBuffer[ cb_getInd_inc(i)];

          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);
          
          //RESPONSE.OUT
          LedOn( LED_TX);
          RS485_SetToTransmit();
          printf( "%s\r", strResponse);
          RS485_SetToReceive();
          return;
        }
        else {
          LogWarn( "AO: #{AA}A{data}: Bad value '%s'", acValue);
          //COMMAND.NOT.ACCEPTED
          LedOn( LED_ERR);

          //RESPONSE.PREPARATION
          sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          RS485_SetToTransmit();
          printf("%s\r", strResponse);
          RS485_SetToReceive();

          return;
        }
      }
      else {
        LogInfo( "AO: Wrong formatted #{AA}A{data} command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }

    }
    else

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'B') {
      // **************************************************************************************
      // #{AA}B{data}
      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 13) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 11)) {

        LogDebug( "AO: Recognized #{AA}B{data} command");

        //PARAM.ANALYZE
        for( i=0; i<6; acValue[ i++] = gl_cpCircleBuffer[ cb_getInd_inc(5 + i)]);

        fValue = atof( acValue);
        if( fValue > -10. && fValue < 10.) {

          LogDebug( "AO: #{AA}B{data}: value '%.3f'", fValue);
          //COMMAND.ACCEPTED
          LedOn( LED_RX);

          //COMMAND.ACTION
          gl_dblAO_calB = fValue;
          save_params( SAVE_MASK_AO);

          //RESPONSE.PREPARATION
          strResponse[0] = '!';
          for( i=2; i<11; i++)
            strResponse[ i-1] = gl_cpCircleBuffer[ cb_getInd_inc(i)];

          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          LedOn( LED_TX);
          RS485_SetToTransmit();
          printf( "%s\r", strResponse);
          RS485_SetToReceive();
          return;
        }
        else {
          LogWarn( "AO: #{AA}B{data}: Bad value '%s'", acValue);
          //COMMAND.NOT.ACCEPTED
          LedOn( LED_ERR);

          //RESPONSE.PREPARATION
          sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          RS485_SetToTransmit();
          printf("%s\r", strResponse);
          RS485_SetToReceive();

          return;
        }
      }
      else {
        LogInfo( "AO: Wrong formatted #{AA}B{data} command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    else

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'C') {
      // **************************************************************************************
      // #{AA}{Cn}{data}

      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 14) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 12)) {

        LogDebug( "AO: Recognized #{AA}{Cn}{data}[CS]: set current value command");

        //PARAM.ANALYZE
        cByte = gl_cpCircleBuffer[ cb_getInd_inc(5)];
        if( cByte >= '0' && cByte <= '4') {
          nChannel = cByte - '0';
          LogDebug( "AO: #{AA}{Cn}{data}: channel #%d", nChannel);

          for( i=0; i<5; acValue[ i++] = gl_cpCircleBuffer[ cb_getInd_inc(6 + i)]);

          fValue = atof( acValue);
          if( fValue >= 0. && fValue <= 5.) {

            LogDebug( "AO: #{AA}{Cn}{data}: value '%.3f'", fValue);
            //COMMAND.ACCEPTED
            LedOn( LED_RX);

            //COMMAND.ACTION
            fValue = ( fValue - gl_dblAO_calB) / gl_dblAO_calA;
            LogDebug( "AO: #{AA}{Cn}{data}: calibrated value '%.3f'", fValue);

            if( fValue < 0) {
              fValue = 0.;
              LogWarn( "AO: #{AA}{Cn}{data}: calibrated value < 0   Truncating to 0");
            }
            if( fValue > 3.0) {
              fValue = 3.0;
              LogWarn( "AO: #{AA}{Cn}{data}: calibrated value > 3.0   Truncating to 3.0");
            }

            shValueCode = ( unsigned short) 4095.0 * ( fValue / 3.0);
            LogDebug( "AO: #{AA}{Cn}{data}: calibrated value code '0x%04X'", shValueCode);

            /*
            LogDebug( "AO: #AACn(data): DAC0CON=0x%02x", DAC0CON);
            LogDebug( "AO: #AACn(data): DAC0DAT=0x%08x", DAC0DAT);
            LogDebug( "AO: #AACn(data): DAC1CON=0x%02x", DAC1CON);
            LogDebug( "AO: #AACn(data): DAC1DAT=0x%08x", DAC1DAT);
            LogDebug( "AO: #AACn(data): DAC2CON=0x%02x", DAC2CON);
            LogDebug( "AO: #AACn(data): DAC2DAT=0x%08x", DAC2DAT);
            LogDebug( "AO: #AACn(data): DAC3CON=0x%02x", DAC3CON);
            LogDebug( "AO: #AACn(data): DAC3DAT=0x%08x", DAC3DAT);
            */
            
            switch( nChannel) {
              case 0: DAC0DAT = shValueCode << 16; break;
              case 1: DAC1DAT = shValueCode << 16; break;
              case 2: DAC2DAT = shValueCode << 16; break;
              case 3:
                GP0DAT &= ~(1 << (16 + 1));  //DAC4-5 switch to DAC4     (p0.1) = 0
                DAC3DAT = shValueCode << 16;
              break;
              case 4:
                GP0DAT |= 1 << (16 + 1);     //DAC4-5 switch to DAC5     (p0.1) = 1
                DAC3DAT = shValueCode << 16;
              break;
            }

            /*
            LogDebug( "AO: #AACn(data): nChannel=%d", nChannel);
            LogDebug( "AO: #AACn(data): DAC0CON=0x%02x", DAC0CON);
            LogDebug( "AO: #AACn(data): DAC0DAT=0x%08x", DAC0DAT);
            LogDebug( "AO: #AACn(data): DAC1CON=0x%02x", DAC1CON);
            LogDebug( "AO: #AACn(data): DAC1DAT=0x%08x", DAC1DAT);
            LogDebug( "AO: #AACn(data): DAC2CON=0x%02x", DAC2CON);
            LogDebug( "AO: #AACn(data): DAC2DAT=0x%08x", DAC2DAT);
            LogDebug( "AO: #AACn(data): DAC3CON=0x%02x", DAC3CON);
            LogDebug( "AO: #AACn(data): DAC3DAT=0x%08x", DAC3DAT);
            */
            gl_paushCurrentDacValues[ nChannel] = shValueCode;

            //RESPONSE.PREPARATION
            strResponse[ 0] = '!';
            for( i = 2; i < 12; i++)
              strResponse[ i - 1] = gl_cpCircleBuffer[ cb_getInd_inc(i)];

            if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

            //RESPONSE.OUT
            LedOn( LED_TX);
            RS485_SetToTransmit();
            printf( "%s\r", strResponse);
            RS485_SetToReceive();
            return;
          }
          else {
            LogWarn( "AO: #{AA}{Cn}{data}: Bad value '%s'", acValue);
            //COMMAND.NOT.ACCEPTED
            LedOn( LED_ERR);

            //RESPONSE.PREPARATION
            sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
            if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

            //RESPONSE.OUT
            RS485_SetToTransmit();
            printf("%s\r", strResponse);
            RS485_SetToReceive();

            return;
          }

        }
        else {
          LogWarn( "AO: #{AA}{Cn}{data}: Bad channel value '%c'", cByte);
          //COMMAND.NOT.ACCEPTED
          LedOn( LED_ERR);

          //RESPONSE.PREPARATION
          sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          RS485_SetToTransmit();
          printf("%s\r", strResponse);
          RS485_SetToReceive();

          return;
        }
      }
      else {
        LogInfo( "AO: Wrong formatted #{AA}{Cn}{data} command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    // **************************************************************************************
    // #AASCn(data)
    else
      
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'S') {

      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 15) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 13)) {

        LogDebug( "AO: Recognized #AASCn(data): set startup value command");

        //PARAM.ANALYZE
                
        
        cByte = gl_cpCircleBuffer[ cb_getInd_inc(6)];
        if( cByte >= '0' && cByte <= '4') {
          nChannel = cByte - '0';
          LogDebug( "AO: #AASCn(data): channel #%d", nChannel);

          for( i=0; i<5; acValue[ i++] = gl_cpCircleBuffer[ cb_getInd_inc( 7 + i)]);

          fValue = atof( acValue);
          if( fValue >= 0. && fValue <= 5.) {
            
            LogDebug( "AO: #AASCn(data): value '%.3f'", fValue);
            //COMMAND.ACCEPTED
            LedOn( LED_RX);

            //COMMAND.ACTION
            fValue =  ( fValue - gl_dblAO_calB) / gl_dblAO_calA;
            LogDebug( "AO: #AASCn(data): calibrated value '%.3f'", fValue);

            if( fValue < 0) {
              fValue = 0.;
              LogWarn( "AO: #AACn(data): calibrated value < 0   Truncating to 0");
            }
            if( fValue > 3.0) {
              fValue = 3.0;
              LogWarn( "AO: #AACn(data): calibrated value > 3.0   Truncating to 3.0");
            }
            
            shValueCode = ( unsigned short) 4095.0 * ( fValue / 3.0);

            gl_paushStartUpDacValues[ nChannel] = shValueCode;
            LogDebug( "AO: #AASCn(data): calibrated value code 0x%04X", gl_paushStartUpDacValues[ nChannel]);

            save_params( SAVE_MASK_AO);

            //RESPONSE.PREPARATION
            strResponse[ 0] = '!';
            for( i=2; i<13; i++)
              strResponse [ i - 1] = gl_cpCircleBuffer[ cb_getInd_inc(i)];

            if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

            //RESPONSE.OUT
            LedOn( LED_TX);
            RS485_SetToTransmit();
            printf( "%s\r", strResponse);
            RS485_SetToReceive();

            return;
          }
          else {
            LogWarn( "AO: Bad value '%s'", acValue);
            //COMMAND.NOT.ACCEPTED
            LedOn( LED_ERR);

            //RESPONSE.PREPARATION
            sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
            if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

            //RESPONSE.OUT
            RS485_SetToTransmit();
            printf("%s\r", strResponse);
            RS485_SetToReceive();

            return;
          }

        }
        else {
          LogWarn( "AO: Bad channel value '%c'", cByte);
          //COMMAND.NOT.ACCEPTED
          LedOn( LED_ERR);

          //RESPONSE.PREPARATION
          sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          RS485_SetToTransmit();
          printf("%s\r", strResponse);
          RS485_SetToReceive();

          return;
        }
      }
      else {
        LogInfo( "AO: Wrong formatted #AASCn(data) command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    else {
      LogInfo( "AO: Unknown #command.");
      //COMMAND.NOT.ACCEPTED
      LedOn( LED_ERR);

      //RESPONSE.PREPARATION
      sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
      if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

      //RESPONSE.OUT
      RS485_SetToTransmit();
      printf("%s\r", strResponse);
      RS485_SetToReceive();

      return;
    }
  }
  else if( gl_cpCircleBuffer[ cb_getInd_inc(1)] == '$' ) {
    // **************************************************************************************
    //Команды запросов $AA

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '6' && gl_cpCircleBuffer[ cb_getInd_inc(5)] == 'C') {
      // **************************************************************************************
      //$AA6Cn
      
      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 9) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 7)) {

        LogDebug( "AO: Recognized $AA6Cn: get current value command");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //COMMAND.ACTION
        cByte = gl_cpCircleBuffer[ cb_getInd_inc(6)];
        if( cByte >= '0' && cByte <= '4') {
          nChannel = cByte - '0';
          LogDebug( "AO: $AA6Cn(data): channel #%d", nChannel);

          fValue = ( gl_paushCurrentDacValues[ nChannel] / 4095.) * 3.0;
          LogDebug( "AO: $AA6Cn(data): value = %03f", fValue);

          fValue = fValue * gl_dblAO_calA + gl_dblAO_calB;
          LogDebug( "AO: $AA6Cn(data): calibrated value = %03f", fValue);

          //RESPONSE.PREPARATION
          strResponse[ 0] = '!';
          strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
          strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];

          if( fValue >= 0)
            sprintf( &strResponse[3], "+%.03f", fValue);
          else
            sprintf( &strResponse[3], "%.03f", fValue);

          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          LedOn( LED_TX);
          RS485_SetToTransmit();
          printf( "%s\r", strResponse);
          RS485_SetToReceive();
          return;

        }
        else {
          LogWarn( "AO: $AA6Cn(data): Bad channel value '%c'", cByte);
          //COMMAND.NOT.ACCEPTED
          LedOn( LED_ERR);

          //RESPONSE.PREPARATION
          sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          RS485_SetToTransmit();
          printf("%s\r", strResponse);
          RS485_SetToReceive();

          return;
        }
      }
      else {
        LogInfo( "AO: Wrong formatted $AA6Cn(data) command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    else
    
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'A') {
      // *************************************************************************
      // ${AA}A[CS]<cr>
      LogDebug( "AO: Recognized ${AA}A[CS]<cr>: req A-calib-coeff command");

      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 5)) {

        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];
        sprintf( &strResponse[3], "%.3f", gl_dblAO_calA);
      
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "AO: Wrong formatted ${AA}A[CS] command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'B') {
      // *************************************************************************
      // ${AA}B[CS]<cr>
      LogDebug( "AO: Recognized ${AA}B[CS]<cr>: req B-calib-coeff command");

      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 5)) {

        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];
        sprintf( &strResponse[3], "%+.3f", gl_dblAO_calB);

        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "AO: Wrong formatted ${AA}B command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'D' && gl_cpCircleBuffer[ cb_getInd_inc(5)] == 'C') {
      // **************************************************************************************
      //$AADCn
      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 9) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 7)) {

        LogDebug( "AO: Recognized ${AA}DCn: get startup value for channel command");

        //PARAM.ANALYZE
        


        cByte = gl_cpCircleBuffer[ cb_getInd_inc(6)];
        if( cByte >= '0' && cByte <= '4') {
          //COMMAND.ACCEPTED
          LedOn( LED_RX);

          //COMMAND.ACTION
          nChannel = cByte - '0';
          LogDebug( "AO: $AADCn(data): channel #%d", nChannel);

          fValue = ( gl_paushStartUpDacValues[ nChannel] / 4095.) * 3.0;
          LogDebug( "AO: $AADCn(data): value = %03f", fValue);

          fValue = fValue * gl_dblAO_calA + gl_dblAO_calB;
          LogDebug( "AO: $AADCn(data): calibrated value = %03f", fValue);

          //RESPONSE.PREPARATION
          strResponse[ 0] = '!';
          strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
          strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];

          sprintf( &strResponse[3], "%+.03f", fValue);

          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);
          
          //RESPONSE.OUT
          LedOn( LED_TX);
          RS485_SetToTransmit();
          printf( "%s\r", strResponse);
          RS485_SetToReceive();
          return;

        }
        else {
          LogWarn( "AO: $AADCn(data): Bad channel value '%c'", cByte);
          //COMMAND.NOT.ACCEPTED
          LedOn( LED_ERR);

          //RESPONSE.PREPARATION
          sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
          if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

          //RESPONSE.OUT
          RS485_SetToTransmit();
          printf("%s\r", strResponse);
          RS485_SetToReceive();

          return;
        }
      }
      else {
        LogInfo( "AO: Wrong formatted $AADCn(data) command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      
    }
    else

    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == 'M') {
      // **************************************************************************************
      //$AAM
      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 5)) {

        LogDebug( "AO: Recognized ${AA}M: Get module name command");
        //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';
        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];
        strResponse[ 3] = 'D';
        strResponse[ 4] = 'A';
        strResponse[ 5] = 'C';

        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "AO: Wrong formatted $AAM command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    
    if( gl_cpCircleBuffer[ cb_getInd_inc(4)] == '2') {
      // **************************************************************************************
      //$AA2
      if( ( gl_cCheckSummUsageAO  && nFirstCommandLen == 7) ||
          ( !gl_cCheckSummUsageAO && nFirstCommandLen == 5)) {
            
        LogDebug( "AO: Recognized ${AA}2: Config request command");
         //COMMAND.ACCEPTED
        LedOn( LED_RX);

        //RESPONSE.PREPARATION
        strResponse[ 0] = '!';

        strResponse[ 1] = gl_cpCircleBuffer[ cb_getInd_inc( 2)];    //AA
        strResponse[ 2] = gl_cpCircleBuffer[ cb_getInd_inc( 3)];    //AA

        strResponse[ 3] = '0';                              //TT
        strResponse[ 4] = '0';                              //TT

        strResponse[ 5] = '0';                              //CC
        strResponse[ 6] = '0';                              //CC

        strResponse[ 7] = gl_cCheckSummUsageAO ? '4':'0';   //FF
        strResponse[ 8] = '0';                              //FF

        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        LedOn( LED_TX);
        RS485_SetToTransmit();
        printf( "%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
      else {
        LogInfo( "AO: Wrong formatted $AA2 command.");
        //COMMAND.NOT.ACCEPTED
        LedOn( LED_ERR);

        //RESPONSE.PREPARATION
        sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
        if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

        //RESPONSE.OUT
        RS485_SetToTransmit();
        printf("%s\r", strResponse);
        RS485_SetToReceive();

        return;
      }
    }
    
    LogInfo( "DI: Unknown $command.");
    //COMMAND.NOT.ACCEPTED
    LedOn( LED_ERR);

    //RESPONSE.PREPARATION
    sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
    if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

    //RESPONSE.OUT
    RS485_SetToTransmit();
    printf("%s\r", strResponse);
    RS485_SetToReceive();

    return;
  }

  LogDebug( "AO: Unknown command.");
  //COMMAND.NOT.ACCEPTED
  LedOn( LED_ERR);

  //RESPONSE.PREPARATION
  sprintf( strResponse, "?%02X", gl_cAddrAnalogueOutput);
  if( gl_cCheckSummUsageAO) AddCheckSumm( strResponse);

  //RESPONSE.OUT
  RS485_SetToTransmit();
  printf("%s\r", strResponse);
  RS485_SetToReceive();

  return;
}
