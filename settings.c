#include <stdio.h>
#include "settings.h"
#include "errors.h"
#include "flashEE.h"
#include "logger.h"
#include "debug.h"

//declared in Main.c
extern char gl_c_EmergencyCode;

//flash-stored params declared in Main.c
extern unsigned short gl_ushBaseSettingsPage;   //стартовая страница для хранения параметров (DI=BASE, DO=BASE+1page, AI=BASE+2page, AO=BASE+3page)

extern unsigned char gl_cAddrDigitalInput;     //адрес блока цифровых входов      (GPI)  TS  DI
extern unsigned char gl_cCheckSummUsageDI;     //использование CheckSumm

extern unsigned char  gl_cAddrDigitalOutput;    //адрес блока цифровых выходов     (GPO) TU  DO
extern unsigned char  gl_cCheckSummUsageDO;     //использование CheckSumm
extern unsigned short gl_ushStartupValueDoL;    //стартовые значения каналов цифровых выходов (младшие 16 каналов)
extern unsigned short gl_ushStartupValueDoH;    //стартовые значения каналов цифровых выходов (старшие 16 каналов)

extern unsigned char gl_cAddrAnalogueInput;    //адрес блока аналоговых входов    (АЦП)        TI  AI
extern unsigned char gl_cCheckSummUsageAI;     //использование CheckSumm
extern double        gl_dblAI_calA;            //A-коэффициент калибровки

extern unsigned char  gl_cAddrAnalogueOutput;   //адрес блока аналоговых выходов  (ЦАП)            AO
extern unsigned char  gl_cCheckSummUsageAO;     //использование CheckSumm
extern unsigned short *gl_paushStartUpDacValues;//стартовые состояния каналов
extern double         gl_dblAO_calA;            //A-коэффициент калибровки
extern double         gl_dblAO_calB;            //B-коэффициент калибровки


void check_params( void) {
  char bOneMoreRound;

  //****************************************************
  //AI ADC
  //AI ADC.Адрес модуля
  //gl_cAddrAnalogueInput can be any [0x00-0xfe] value
  if( gl_cAddrAnalogueInput == 0xFF) gl_cAddrAnalogueInput++;

  //AI ADC.Использование CheckSumm
  if( gl_cCheckSummUsageAI != 1) gl_cCheckSummUsageAI = 0;

  //AI ADC.Калибровочный коэффициент A
  if( gl_dblAI_calA > 10.) gl_dblAI_calA = 1.;


  //****************************************************
  //AO DAC
  //AO DAC.Адрес модуля
  //gl_cAddrAnalogueOutput can be any [0x00-0xfe] value, but not equal to gl_cAddrAnalogueInput
  bOneMoreRound = 1;
  do {
    if( gl_cAddrAnalogueOutput == 0xFF) gl_cAddrAnalogueOutput++;

    if( gl_cAddrAnalogueOutput == gl_cAddrAnalogueInput) {
      gl_cAddrAnalogueOutput = (++gl_cAddrAnalogueOutput) % 0xFE;
      continue;
    }

    bOneMoreRound = 0;
  } while( bOneMoreRound);

  //AO DAC.Использование CheckSumm
  if( gl_cCheckSummUsageAO != 1) gl_cCheckSummUsageAO = 0;

  //AO DAC.Стартовое значение канала AO.1
  if( gl_paushStartUpDacValues[0] == 0xFFFF) gl_paushStartUpDacValues[0] = 0;

  //AO DAC.Стартовое значение канала AO.2
  if( gl_paushStartUpDacValues[1] == 0xFFFF) gl_paushStartUpDacValues[1] = 0;

  //AO DAC.Стартовое значение канала AO.3
  if( gl_paushStartUpDacValues[2] == 0xFFFF) gl_paushStartUpDacValues[2] = 0;

  //AO DAC.Стартовое значение канала AO.4
  if( gl_paushStartUpDacValues[3] == 0xFFFF) gl_paushStartUpDacValues[3] = 0;

  //AO DAC.Стартовое значение канала AO.5
  if( gl_paushStartUpDacValues[4] == 0xFFFF) gl_paushStartUpDacValues[4] = 0;

  //AO DAC.Калибровочный коэффициент A
  if( gl_dblAO_calA > 10.) gl_dblAO_calA = 1.;

  //AO DAC.Калибровочный коэффициент B
  if( gl_dblAO_calB < -10.) gl_dblAO_calB = -10.;
  if( gl_dblAO_calB > 10.)  gl_dblAO_calB = 10.;


  //****************************************************
  //DI GPI
  //DI GPI.Адрес модуля
  //gl_cAddrDigitalInput can be any [0x00-0xfe] value, but not equal to gl_cAddrAnalogueInput, gl_cAddrAnalogueOutput
  bOneMoreRound = 1;
  do {
    if( gl_cAddrDigitalInput == 0xFF) gl_cAddrDigitalInput++;

    if( gl_cAddrDigitalInput == gl_cAddrAnalogueInput) {
      gl_cAddrDigitalInput = (++gl_cAddrDigitalInput) % 0xFE;
      continue;
    }
    
    if( gl_cAddrDigitalInput == gl_cAddrAnalogueOutput) {
      gl_cAddrDigitalInput = (++gl_cAddrDigitalInput) % 0xFE;
      continue;
    }

    bOneMoreRound = 0;
  } while( bOneMoreRound);
  
  //DI GPO.Использование CheckSumm
  if( gl_cCheckSummUsageDI != 1) gl_cCheckSummUsageDI = 0;


  //****************************************************
  //DO GPO
  //DO GPO.Адрес модуля
  //gl_cAddrDigitalOutput can be any [0x00-0xfe] value, but not equal to gl_cAddrAnalogueInput, gl_cAddrAnalogueOutput, gl_cAddrDigitalInput
  bOneMoreRound = 1;
  do {
    if( gl_cAddrDigitalOutput == 0xFF) gl_cAddrDigitalOutput++;

    if( gl_cAddrDigitalOutput == gl_cAddrAnalogueInput) {
      gl_cAddrDigitalOutput = (++gl_cAddrDigitalOutput) % 0xFE;
      continue;
    }
    
    if( gl_cAddrDigitalOutput == gl_cAddrAnalogueOutput) {
      gl_cAddrDigitalOutput = (++gl_cAddrDigitalOutput) % 0xFE;
      continue;
    }
    
    if( gl_cAddrDigitalOutput == gl_cAddrDigitalInput) {
      gl_cAddrDigitalOutput = (++gl_cAddrDigitalOutput) % 0xFE;
      continue;
    }

    bOneMoreRound = 0;
  } while( bOneMoreRound);

  //DO GPO.Использование CheckSumm
  if( gl_cCheckSummUsageDO != 1) gl_cCheckSummUsageDO = 0;
  
  //DO GPO.Стартовые значения младших каналов [0-15]
  if( gl_ushStartupValueDoL == 0xFFFF) gl_ushStartupValueDoL = 0;
  
  //DO GPO.Стартовые значения старших каналов [16-Rest]
  if( gl_ushStartupValueDoH == 0xFFFF) gl_ushStartupValueDoH = 0;
}

int load_params_ai( void) {
  unsigned short ushValue;

  //****************************************************
  //AI ADC
  //AI ADC.адрес блока аналоговых входов   (АЦП)        TI  AI
  if( flashEE_load_short( ADDR_AI, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cAddrAnalogueInput = ( unsigned char) ushValue;

  //AI ADC.использование CS
  if( flashEE_load_short( ADDR_AI_CS_USAGE, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cCheckSummUsageAI = ( unsigned char) ushValue;


  //AI ADC.калибровочный коэффициент А
  if( flashEE_load_short( ADDR_AI_COEFF_A, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_dblAI_calA = ushValue / 1000.;
  
  return 0;
}

int load_params_ao( void) {
  unsigned short ushValue;
  //signed short shValue;

  //****************************************************
  //AO DAC
  //AO DAC.адрес блока аналоговых выходов  (ЦАП)      AO
  if( flashEE_load_short( ADDR_AO, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cAddrAnalogueOutput = ( unsigned char) ushValue;

  //AO DAC.использование CS
  if( flashEE_load_short( ADDR_AO_CS_USAGE, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cCheckSummUsageAO = ( unsigned char) ushValue;

  //AO DAC.Стартовое значение канала AO.1
  if( flashEE_load_short( ADDR_AO_STARTUP_C0, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_paushStartUpDacValues[0] = ( unsigned short) ushValue;

  //AO DAC.Стартовое значение канала AO.2
  if( flashEE_load_short( ADDR_AO_STARTUP_C1, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_paushStartUpDacValues[1] = ( unsigned short) ushValue;

  //AO DAC.Стартовое значение канала AO.3
  if( flashEE_load_short( ADDR_AO_STARTUP_C2, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_paushStartUpDacValues[2] = ( unsigned short) ushValue;

  //AO DAC.Стартовое значение канала AO.4
  if( flashEE_load_short( ADDR_AO_STARTUP_C3, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_paushStartUpDacValues[3] = ( unsigned short) ushValue;

  //AO DAC.Стартовое значение канала AO.5
  if( flashEE_load_short( ADDR_AO_STARTUP_C4, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_paushStartUpDacValues[4] = ( unsigned short) ushValue;

  //AO DAC.калибровочный коэффициент А
  if( flashEE_load_short( ADDR_AO_COEFF_A, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_dblAO_calA = ushValue / 1000.;
  
  //AO DAC.калибровочный коэффициент B
  if( flashEE_load_short( ADDR_AO_COEFF_B, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  if( ushValue == 0xFFFF) ushValue = 10000;
  gl_dblAO_calB = ( ushValue / 1000.) - 10.;  
  LogDebug( "load_params_ao: ADDR_AO_COEFF_B: USH: %d (0x%04x)", ushValue, ushValue);
  LogDebug( "load_params_ao: ADDR_AO_COEFF_B: DBL: %.03f", gl_dblAO_calB);
  
  return 0;
}

int load_params_di( void) {
  unsigned short ushValue;

  //****************************************************
  //Загрузка настроек модуля цифровых входов DI GPI TS
  //Адрес блока цифровых входов
  if( flashEE_load_short( ADDR_DI, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cAddrDigitalInput = ( unsigned char) ushValue;

  //использование CS
  if( flashEE_load_short( ADDR_DI_CS_USAGE, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cCheckSummUsageDI = ( unsigned char) ushValue;

  return 0;
}

int load_params_do( void) {
  unsigned short ushValue;

  //****************************************************
  //Загрузка настроек модуля цифровых выходов DO GPO TU
  //Адрес блока цифровых выходов
  if( flashEE_load_short( ADDR_DO, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cAddrDigitalOutput = ( unsigned char) ushValue;

  //использование CS
  if( flashEE_load_short( ADDR_DO_CS_USAGE, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_cCheckSummUsageDO = ( unsigned char) ushValue;

  //Стартовые состояния младших каналов [0-15]
  if( flashEE_load_short( ADDR_DO_STARTUP_VALUESL, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_ushStartupValueDoL = ( unsigned short) ushValue;
  
  //Стартовые состояния старших каналов [16-Rest]
  if( flashEE_load_short( ADDR_DO_STARTUP_VALUESH, &ushValue)) return ERROR_FLASH_LOAD_PARAMS_FAIL;
  gl_ushStartupValueDoH = ( unsigned short) ushValue;
  
  return 0;
}


int load_params_basePage( void) {
  //****************************************************
  //Загрузка страницы, начиная с которой хранятся параметры
  //Адрес блока цифровых выходов
  if( flashEE_load_short( ADDR_PAGE1, &gl_ushBaseSettingsPage)) return ERROR_FLASH_LOAD_PARAMS_FAIL;

  if( gl_ushBaseSettingsPage == 0xFFFF) {
    gl_ushBaseSettingsPage = 0;
  }
  
  return 0;
}

void load_params( void) {
  //unsigned short ushValue;
  int nResult;
  
  /*
  nResult = load_params_basePage();
  if( nResult != 0) { gl_c_EmergencyCode = ERROR_FLASH_LOAD_PARAMS_FAIL; return; }
  */

  nResult = load_params_di();
  if( nResult != 0) { gl_c_EmergencyCode = ERROR_FLASH_LOAD_PARAMS_FAIL; return; }

  nResult = load_params_do();
  if( nResult != 0) { gl_c_EmergencyCode = ERROR_FLASH_LOAD_PARAMS_FAIL; return; }

  nResult = load_params_ai();
  if( nResult != 0) { gl_c_EmergencyCode = ERROR_FLASH_LOAD_PARAMS_FAIL; return; }

  nResult = load_params_ao();
  if( nResult != 0) { gl_c_EmergencyCode = ERROR_FLASH_LOAD_PARAMS_FAIL; return; }


  check_params();
  
  LogDebug( "AI addr: %d", gl_cAddrAnalogueInput);
  LogDebug( "AO addr: %d", gl_cAddrAnalogueOutput);
  LogDebug( "DI addr: %d", gl_cAddrDigitalInput);
  LogDebug( "DO addr: %d", gl_cAddrDigitalOutput);
}

void save_params_ai( void) {
  //**************************************************
  //БЛОК АНАЛОГОВЫХ ВХОДОВ
  //**************************************************

  if( flashEE_erase_page( ADDR_PAGE3)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }

  //AI ADC.адрес блока аналоговых входов    (АЦП)        TI  AI
  if( flashEE_save_short( ADDR_AI, ( unsigned short) gl_cAddrAnalogueInput)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AI ADC.использование CheckSumm
  if( flashEE_save_short( ADDR_AI_CS_USAGE, ( unsigned short) gl_cCheckSummUsageAI)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AI ADC.калибровочный коэффициент А
  if( flashEE_save_short( ADDR_AI_COEFF_A, ( unsigned short) ( gl_dblAI_calA * 1000.))) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
}

void save_params_ao( void) {
  double dblValue;
  unsigned short ushValue;
  //**************************************************
  //БЛОК АНАЛОГОВЫХ ВЫХОДОВ
  //**************************************************

  if( flashEE_erase_page( ADDR_PAGE4)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }

  //AO DAC.адрес блока аналоговых выходов   (ЦАП)            AO
  if( flashEE_save_short( ADDR_AO, ( unsigned short) gl_cAddrAnalogueOutput)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.использование CheckSumm
  if( flashEE_save_short( ADDR_AO_CS_USAGE, ( unsigned short) gl_cCheckSummUsageAO)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.Стартовое значение канала AO.1
  if( flashEE_save_short( ADDR_AO_STARTUP_C0, ( unsigned short) gl_paushStartUpDacValues[0])) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.Стартовое значение канала AO.2
  if( flashEE_save_short( ADDR_AO_STARTUP_C1, ( unsigned short) gl_paushStartUpDacValues[1])) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.Стартовое значение канала AO.3
  if( flashEE_save_short( ADDR_AO_STARTUP_C2, ( unsigned short) gl_paushStartUpDacValues[2])) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.Стартовое значение канала AO.4
  if( flashEE_save_short( ADDR_AO_STARTUP_C3, ( unsigned short) gl_paushStartUpDacValues[3])) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.Стартовое значение канала AO.5
  if( flashEE_save_short( ADDR_AO_STARTUP_C4, ( unsigned short) gl_paushStartUpDacValues[4])) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.калибровочный коэффициент А
  if( flashEE_save_short( ADDR_AO_COEFF_A, ( unsigned short) ( gl_dblAO_calA * 1000.))) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //AO DAC.калибровочный коэффициент B
  dblValue = ( ( gl_dblAO_calB + 10.) * 1000.);
  ushValue = ( unsigned short) dblValue;
  
  LogDebug( "save_params_ao: ADDR_AO_COEFF_B: dblValue: %.03f", dblValue);
  LogDebug( "save_params_ao: ADDR_AO_COEFF_B: ushValue: %d (0x%04X)", ushValue, ushValue);
  
  if( flashEE_save_short( ADDR_AO_COEFF_B, ushValue)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
}

void save_params_di( void) {
  //**************************************************
  //БЛОК ЦИФРОВЫХ ВХОДОВ
  //**************************************************

  if( flashEE_erase_page( ADDR_PAGE1)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }

  //адрес блока цифрорвых входов     (реле вход)  TS  DI
  if( flashEE_save_short( ADDR_DI, ( unsigned short) gl_cAddrDigitalInput)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //использование CheckSumm
  if( flashEE_save_short( ADDR_DI_CS_USAGE, ( unsigned short) gl_cCheckSummUsageDI)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
}

void save_params_do( void) {
  //**************************************************
  //БЛОК ЦИФРОВЫХ ВЫХОДОВ
  //**************************************************

  if( flashEE_erase_page( ADDR_PAGE2)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }

  //адрес блока цифровых выходов (реле выход) TU  DO
  if( flashEE_save_short( ADDR_DO, ( unsigned short) gl_cAddrDigitalOutput)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }

  //использование CheckSumm
  if( flashEE_save_short( ADDR_DO_CS_USAGE, ( unsigned short) gl_cCheckSummUsageDO)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }

  //Стартап-значения младших каналов [0-15]
  if( flashEE_save_short( ADDR_DO_STARTUP_VALUESL, ( unsigned short) gl_ushStartupValueDoL)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
  
  //Стартап-значения старших каналов [16-Rest]
  if( flashEE_save_short( ADDR_DO_STARTUP_VALUESH, ( unsigned short) gl_ushStartupValueDoH)) {
    gl_c_EmergencyCode = ERROR_FLASH_SAVE_PARAMS_FAIL;
    return;
  }
}



void save_params( int nMask) {
  if( nMask & SAVE_MASK_DI) save_params_di();
  if( nMask & SAVE_MASK_DO) save_params_do();
  if( nMask & SAVE_MASK_AI) save_params_ai();
  if( nMask & SAVE_MASK_AO) save_params_ao();
}

void print_params_di() {
  LogDebug( "@S: DI: ADDR_DI:                 0x%02x", gl_cAddrDigitalInput);
  LogDebug( "@S: DI: ADDR_DI_CS_USAGE:        0x%02x", gl_cCheckSummUsageDI);
}

void print_params_do() {
  LogDebug( "@S: DO: ADDR_DO:                 0x%02x", gl_cAddrDigitalOutput);
  LogDebug( "@S: DO: ADDR_DO_CS_USAGE:        0x%02x", gl_cCheckSummUsageDO);

  LogDebug( "@S: AO: ADDR_DO_STARTUP_VALUESL: 0x%04x", gl_ushStartupValueDoL);
  LogDebug( "@S: AO: ADDR_DO_STARTUP_VALUESH: 0x%04x", gl_ushStartupValueDoH);
}

void print_params_ai() {
  LogDebug( "@S: AI: ADDR_AI:                 0x%02x", gl_cAddrAnalogueInput);
  LogDebug( "@S: AI: ADDR_AI_CS_USAGE:        0x%02x", gl_cCheckSummUsageAI);
  LogDebug( "@S: AI: ADDR_AI_COEFF_A:         %.03f",  gl_dblAI_calA);
}

void print_params_ao() {
  LogDebug( "@S: AO: ADDR_AO:                 0x%02x", gl_cAddrAnalogueOutput);
  LogDebug( "@S: AO: ADDR_AO_CS_USAGE:        0x%02x", gl_cCheckSummUsageAO);

  LogDebug( "@S: AO: ADDR_AO_STARTUP_C0:      0x%04x", gl_paushStartUpDacValues[0]);
  LogDebug( "@S: AO: ADDR_AO_STARTUP_C1:      0x%04x", gl_paushStartUpDacValues[1]);
  LogDebug( "@S: AO: ADDR_AO_STARTUP_C2:      0x%04x", gl_paushStartUpDacValues[2]);
  LogDebug( "@S: AO: ADDR_AO_STARTUP_C3:      0x%04x", gl_paushStartUpDacValues[3]);
  LogDebug( "@S: AO: ADDR_AO_STARTUP_C4:      0x%04x", gl_paushStartUpDacValues[4]);
  LogDebug( "@S: AO: ADDR_AO_STARTUP_C5:      0x%04x", gl_paushStartUpDacValues[5]);

  LogDebug( "@S: AO: ADDR_AO_COEFF_A:         %.03f",  gl_dblAO_calA);
  LogDebug( "@S: AO: ADDR_AO_COEFF_B:         %.03f",  gl_dblAO_calB);
}

void print_params(int nMask) {
  if( nMask & SAVE_MASK_DI) print_params_di();
  if( nMask & SAVE_MASK_DO) print_params_do();
  if( nMask & SAVE_MASK_AI) print_params_ai();
  if( nMask & SAVE_MASK_AO) print_params_ao();
}
