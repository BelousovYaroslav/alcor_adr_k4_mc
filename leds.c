#include <ADuC7026.h>
#include "settings.h"

extern int gl_nT1ValRxLedDown, gl_nT1ValTxLedDown, gl_nT1ValErrLedDown;

void LedOn( int nLed) {
  switch( nLed) {
    case LED_RX:
      GP4DAT &= ~(1 << (16 + 5));                         //RxLED on       (p4.5) = 0
      gl_nT1ValRxLedDown = (T1VAL + T1LD - 3276) % T1LD;  //Lock RxLed down event
    break;
    case LED_TX:
      GP4DAT &= ~(1 << (16 + 4));                         //TxLED on       (p4.4) = 0
      gl_nT1ValTxLedDown = (T1VAL + T1LD - 3276) % T1LD;  //Lock TxLed down event
    break;
    case LED_ERR:
      GP4DAT &= ~(1 << (16 + 3));                         //ErrLED on      (p4.3) = 0
      gl_nT1ValErrLedDown = (T1VAL + T1LD - 3276) % T1LD; //Lock ErrLed down event
    break;
  }
}
